\input{../meta.tex}

\title{Lecture 07 --- The Inverse of a Matrix}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{2.2 The Inverse of a Matrix}

\subsection{Invertible Matrices}

\begin{frame}
    \frametitle{The inverse of a number}
    
    The inverse of $5$ is $1/5$ or $5^{-1}$, which satisfies
    \begin{equation*}
        5 \cdot 5^{-1} = 1,
        \qquad
        5^{-1} \cdot 5 = 1.
    \end{equation*}

    \cake{} What is the analogue of $1$ for matrices?
\end{frame}

\begin{frame}
    \frametitle{Invertible matrices}

    An $n \times n$ matrix is \alert{invertible/non-singular} if there is
    matrix $C$ such that
    \begin{equation*}
        AC = I_n, \qquad CA = I_n.
    \end{equation*}
    $C$ is called an \alert{inverse} of $A$.

    \pause{}

    A matrix which is \emph{not} invertible is called \alert{singular}.

    \pause{}

    Any matrices $A$ has \emph{at most} one inverse, which we denote by $A^{-1}$.
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \cake{} When is $A = [a]$ invertible (non-singular)?
%\end{frame}

\begin{frame}
    \frametitle{Think-Pair-Share \sweat{}}

    (a) Suppose that $A B = A C$, where $B$ and $C$ are of size $n \times p$ and $A$ is
    an \emph{invertible} $n \times n$ matrix. Show that $B = C$.

    \pause{}

    (b) Is this still always true when we do \emph{not} assume $A$ is invertible?
    Why?

    \hint{} No need to use any theorem.
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    We have
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            2 & 5 \\ 
            -3 & -7 \\
        \end{bmatrix}
        ,
        \qquad
        A^{-1}
        =
        \begin{bmatrix}
            -7 & -5 \\ 
            3 & 2  \\
        \end{bmatrix}
        \qquad
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{$2 \times 2$ matrices}
    
    \begin{block}{Theorem 4}
        Let $A = \begin{bmatrix}a & b\\c & d\end{bmatrix}$.
        If $a d - b c \ne 0$, then $A$ is invertible,
        and
        \begin{equation*}
            A^{-1} = \frac{1}{ad-bc}
            \begin{bmatrix}
                d & -b \\ -c & a
            \end{bmatrix}
        \end{equation*}
        Otherwise, $A$ is singular.
    \end{block}

    \pause{}

    We call $ad -bc$ the \alert{determinant} of $A$ and write
    \begin{equation*}
        \det A = ad -bc
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Exercise}

    \cake{} The following is \question{}
    \begin{equation*}
        A 
        =
        \begin{bmatrix}
            3 & 2 \\
            9 & 6 \\
        \end{bmatrix}
    \end{equation*}
    because $\det A = \text{\question}$.
\end{frame}

\begin{frame}
    \frametitle{The unique solution}
    
    \begin{block}{Theorem 5}
        If $A$ is invertible, then for each $\bfb \in \dsR^{n}$,
        $A \bfx = \bfb$ has a unique solution $A^{-1} \bfb$.
    \end{block}
\end{frame}

%\begin{frame}
%    \frametitle{Deflection of an elastic beam}
%
%    Let $\bff \in \dsR^{3}$ be the forces applied a beam 
%    and $\bfy \in \dsR^{3}$ be the amount of deflection/movement.
%    It can be shown that
%    \begin{equation*}
%        \bfy = D \bff
%    \end{equation*}
%    where $D$ is the \emph{stiffness matrix}.
%
%    What are the columns of of $D$ and $D^{-1}$?
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{elastic-beam.png}
%    \end{figure}
%
%    \cake{} What does it imply when $D = [\bfzero \quad \bfzero \quad \bfzero]$?
%\end{frame}

\begin{frame}
    \frametitle{Inverses and transposes}
    
    \begin{block}{Theorem 6}
        \begin{enumerate}
            \item[a.] If $A$ is invertible, then $(A^{-1})^{-1} = A$.
            \item[b.] If $A$ and $B$ are both invertible, then $ (AB)^{-1} = B^{-1}
                A^{-1}$.
            \item[c.] If $A$ is invertible, then so is $A^{T}$, and $(A^{T})^{-1} = (A^{-1})^{T}$.
        \end{enumerate}
    \end{block}
\end{frame}

\subsection{Elementary Matrices}%

\begin{frame}
    \frametitle{Elementary matrices}

    An \alert{elementary matrix} is obtained by performing
    \emph{a single} row operation on an identity matrix.

    \pause{}
    
    \cake{} Recall that
    \begin{equation*}
        I_{3}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 1 \\
        \end{bmatrix}
        .
    \end{equation*}
    How to get the following from $I_3$?
    \begin{equation*}
        E_{1}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            -4 & 0 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        E_{2}
        =
        \begin{bmatrix}
            0 & 1 & 0 \\
            1 & 0 & 0 \\
            0 & 0 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        E_{3}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 5 \\
        \end{bmatrix}
        ,
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 5}
    Let 
    \begin{equation*}
        E_{1}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            -4 & 0 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        E_{2}
        =
        \begin{bmatrix}
            0 & 1 & 0 \\
            1 & 0 & 0 \\
            0 & 0 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        E_{3}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 5 \\
        \end{bmatrix}
        ,
    \end{equation*}
    \begin{equation*}
        A = 
        \begin{bmatrix}
            a & b & c\\ 
            d & e & f\\ 
            g & h & j
        \end{bmatrix}.
    \end{equation*}
    \cake{} Then 
    \begin{equation*}
        \only<1>{
            E_{1} A = 
            \begin{bmatrix}
                a & b & c\\ 
                d & e & f\\ 
                g - 4a & h -4b & j - 4c
            \end{bmatrix}
        }
        \only<2>{
            E_{2} A = 
            \begin{bmatrix}
                d & e & f\\ 
                a & b & c\\ 
                g & h & j
            \end{bmatrix}
        }
        \only<3>{
            E_{3} A = 
            \begin{bmatrix}
                a & b & c\\ 
                d & e & f\\ 
                5 g & 5 h & 5 j
            \end{bmatrix}
        }
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Properties of elementary matrices}

    1. Performing an elementary row operation on $A$
    can be written as $EA$, 
    where $E$ is an elementary matrix created by performing the same
    operation on $I_{n}$.

    \pause{}

    2. Each elementary matrix $E$ is invertible since row operations are reversible.

    \pause{}

    \cake{} 
    The inverse of $E$ is another elementary matrix $F$
    \begin{equation*}
        E
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            -4 & 0 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        F
        =
        \begin{bmatrix}
            \_ & \_ & \_ \\
            \_ & \_ & \_ \\
            \_ & \_ & \_ \\
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Invertible matrices}
    
    \begin{block}{Theorem 7}
        $A$ is invertible if and only if $A \sim I_{n}$.

        \pause{}
        In this case, the same row operations reducing $A$ to $I_{n}$ also reduce
        $I_{n}$ to $A^{-1}$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{How to find $A^{-1}$?}

    \begin{block}{An algorithm}
        Row reduce $[A \quad I]$ to reduced echelon form.

        If $A$ is non-singular, then $[A \quad I] \sim [I \quad A^{-1}]$.

        Otherwise $A$ is singular.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 7}
    To find the inverse of
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            0 & 1 & 2\\
            1 & 0 & 3\\
            4 & -3 & 8\\
        \end{bmatrix}
    \end{equation*}
    \pause{}
    reduce
    \begin{equation*}
        \begin{bmatrix}
            A & I
        \end{bmatrix}
        =
        \begin{bmatrix}
            0 & 1 & 2 & 1 & 0 & 0\\
            1 & 0 & 3 & 0 & 1 & 0\\
            4 & -3 & 8 & 0 & 0 & 1\\
        \end{bmatrix}
    \end{equation*}
    \pause{}
    to its reduced echelon form
    \begin{equation*}
        \begin{bmatrix}
            1 & 0 & 0 & -4 & 7 & -2 \\
            0 & 1 & 0 & -2 & 4 & -1 \\
            0 & 0 & 1 & 2 & -2 & 0 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            I & A^{-1}
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{Think-Pair-Share}
%    
%    \think{} What are the inverses of
%    \begin{equation*}
%        \begin{bmatrix}
%            1 & 0 \\
%            1 & 1 \\
%        \end{bmatrix}
%        ,
%        \qquad
%        \begin{bmatrix}
%            1 & 0 & 0 \\
%            1 & 1 & 0 \\
%            1 & 1 & 1 \\
%        \end{bmatrix}
%        ,
%        \qquad
%        \begin{bmatrix}
%            1 & 0 & 0 & 0 \\
%            1 & 1 & 0 & 0 \\
%            1 & 1 & 1 & 0 \\
%            1 & 1 & 1 & 1 \\
%        \end{bmatrix}
%        ,
%    \end{equation*}
%    Can you \emph{guess} the inverse of
%    the corresponding $n \times n$ matrix?
%\end{frame}

\end{document}
