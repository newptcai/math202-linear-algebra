\input{../meta.tex}

\title{Lecture 17 --- Rank}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{4.6 Rank}

\subsection{Row Space}

\begin{frame}
    \frametitle{Row space}
    
    The \alert{row space} of $A$, denote by $\rowspace A$,  is $\colspace A^{T}$.

    Let
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            -2 & -5 & 8 & 0 & -17 \\
            1 & 3 & -5 & 1 & 5 \\
            3 & 11 & -19 & 7 & 1 \\
            1 & 7 & -13 & 5 & -3 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            \bfr_1 \\ \bfr_2 \\ \bfr_3 \\ \bfr_4
        \end{bmatrix}
    \end{equation*}
    Then
    \begin{equation*}
        \rowspace A = \colspace A^{T}
        = \Span{\bfr_{1}, \dots, \bfr_{4}}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Row equivalent}
    
    \begin{block}{Theorem 13}
        If $A \sim B$, then $\rowspace A = \rowspace B$.

        \pause{}

        If $B$ is in echelon form, the nonzero rows of $B$ form a basis of both row
        space.
    \end{block}

    Example ---
    \begin{equation*}
        A =
        \begin{bmatrix}
            1 & 2 \\
            3 & 4 \\
            1 & 2 \\
        \end{bmatrix}
        \sim
        B =
        \begin{bmatrix}
            1 & 2 \\
            0 & -2 \\
            0 & 0 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    
    Consider
    \begin{equation*}
        A=
        \left[
            \begin{array}{ccccc}
                -2 & -5 & 8 & 0 & -17 \\
                1 & 3 & -5 & 1 & 5 \\
                3 & 11 & -19 & 7 & 1 \\
                1 & 7 & -13 & 5 & -3 \\
            \end{array}
        \right]
        \sim
        \left[
            \begin{array}{ccccc}
                1 & 0 & 1 & 0 & 1 \\
                0 & 1 & -2 & 0 & 3 \\
                0 & 0 & 0 & 1 & -5 \\
                0 & 0 & 0 & 0 & 0 \\
            \end{array}
        \right]
        =
        C
    \end{equation*}
    \cake{} Find the bases of $\rowspace A$, $\colspace A$ and $\nulspace A$.
\end{frame}

\subsection{The Rank Theorem}

\begin{frame}
    \frametitle{The rank}
    The \alert{rank} of $A$ is the dimension of the column space of $A$.

    \cake{} What is the rank of $A$?
    \begin{equation*}
        A=
        \left[
            \begin{array}{ccccc}
                -2 & -5 & 8 & 0 & -17 \\
                1 & 3 & -5 & 1 & 5 \\
                3 & 11 & -19 & 7 & 1 \\
                1 & 7 & -13 & 5 & -3 \\
            \end{array}
        \right]
        \sim
        \left[
            \begin{array}{ccccc}
                1 & 0 & 1 & 0 & 1 \\
                0 & 1 & -2 & 0 & 3 \\
                0 & 0 & 0 & 1 & -5 \\
                0 & 0 & 0 & 0 & 0 \\
            \end{array}
        \right]
        =
        C
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The rank theorem}
    
    \begin{block}{Theorem 14}
        Let $A$ be a $m \times n$ matrix.
        Then
        \begin{equation*}
            \rank A 
            = \dim \colspace A
            = \dim \rowspace A
        \end{equation*}
        and
        \begin{equation*}
        \rank{A}
        +
        \dim \nulspace A = n.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 3}
    \cake{} If A is a $7 \times 9$ matrix with a two-dimensional null space, what is the rank of A?

    \think{} Could a $6 \times 9$ matrix have a two-dimensional null space?
\end{frame}

%\begin{frame}
%    \frametitle{Visualize $\rowspace A$ and $\nulspace A$}
%
%    Let
%    \begin{equation}
%        A =
%        \left[
%            \begin{array}{ccc}
%                3 & 0 & -1 \\
%                3 & 0 & -1 \\
%                4 & 0 & 5 \\
%            \end{array}
%        \right]
%    \end{equation}
%
%    From the picture, we can see that $\rowspace A$ and $\nulspace A$ are 
%    perpendicular.
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.6\linewidth]{null-space-row-space.png}
%    \end{figure}
%\end{frame}

%\begin{frame}
%    \frametitle{An application}
%
%    AMPLE 5 A scientist has found two solutions to a homogeneous system of 40 equations
%    in 42 variables.
%    The two solutions are not multiples,
%    and all other solutions can be constructed by adding together appropriate multiples
%    of these two solutions.
%    Can the scientist be certain that an associated nonhomogeneous system (with the same
%    coefficients) has a solution?
%\end{frame}

\begin{frame}
    \frametitle{The Invertible Matrix Theorem}

    \begin{block}{Theorem 8 (Ch.~2)}
    \scriptsize{}
    Let $A$ be $n \times n$.
    The following are equivalent.
    \vspace{-1em}
    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
        \item[c.] $A$ has $n$ pivot positions.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[e.] The columns of $A$ form a linearly independent set.
        \item[f.] The linear transformation $x \mapsto A \bfx$ is one-to-one.
        \item[g.] The equation $A \bfx = $ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[h.] The columns of $A$ spans $\dsR^n$.
        \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
        \item[j.] There is an $n \times n$ matrix C such that $CA = I_n$.
        \item[k.] There is an $n \times n$ matrix D such that $AD = I_n$.
        \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{The Invertible Matrix Theorem (cont.)}

    \begin{block}{Theorem 8 continued}
    \begin{enumerate}
        \item[m.] The columns of $A$ is a basis of $\dsR^{n}$
        \item[n.] $\colspace A = \dsR^{n}$
        \item[o.] $\dim \colspace A = n$
        \item[p.] $\rank A = n$
        \item[q.] $\nulspace A = \{\bfzero\}$
        \item[r.] $\dim \nulspace A = 0$
    \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Proof (1)}
    
    \begin{block}{Theorem 8 continued}
        \begin{enumerate}
            \item[m.] The columns of $A$ is a basis of $\dsR^{n}$
            \item[e.] The columns of $A$ form a linearly independent set.
            \item[h.] The columns of $A$ spans $\dsR^n$.
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Proof (2)}
    
    \begin{block}{Theorem 8 continued}
        \begin{enumerate}
            \item[g.] The equation $A \bfx = $ has at least one solution for each $\bfb$ in $\dsR^n$.
            \item[n.] $\colspace A = \dsR^{n}$
            \item[o.] $\dim \colspace A = n$
            \item[p.] $\rank A = n$
            \item[r.] $\dim \nulspace A = 0$
            \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \end{enumerate}
    \end{block}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \think{}
%    Suppose $A$ is $m \times n$ and $\bfb \in \dsR^m$. 
%    What has to be true about the two numbers 
%    \begin{equation*}
%        \rank [A \, b],
%        \qquad
%        \rank A
%    \end{equation*}
%    in order for $A \bfx = \bfb$ to be consistent?
%\end{frame}

\section{4.7 Change of Basis}

\begin{frame}
    \frametitle{Two bases}

    In the left, $\bfx = 3 \bfb_1 + \bfb_2$.
    In the right, $\bfx = 6 \bfc_1 + \bfc_4$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{change-of-basis-01.png}
        \caption*{Two bases and the same vector}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    Consider two bases 
    $\scB = \{\bfb_1, \bfb_2\}$ 
    and 
    $\scC = \{\bfc_1, \bfc_2\}$, such that
    \begin{equation*}
        \bfb_1 = 4 \bfc_1 + \bfc_2,
        \qquad
        \bfb_2 = -6 \bfc_1 + \bfc_2
        .
    \end{equation*}
    Suppose that
    \begin{equation*}
        \bfx = 3 \bfb_1 + \bfb_2.
    \end{equation*}
    What is $[\bfx]_\scC$?
\end{frame}

\begin{frame}
    \frametitle{Change of bases}
    
    \begin{block}{Theorem 15}
        Let
        $\scB = \{\bfb_1, \bfb_2, \dots, \bfb_n\}$ 
        and 
        $\scC = \{\bfc_1, \bfc_2, \dots, \bfc_n\}$
        be two bases of $V$.
        Then
        \begin{equation*}
            [\bfx]_{\scC}
            =
            \underset{\scC\leftarrow\scB}{P}
            \cdot
            [\bfx]_{\scB}
            ,
        \end{equation*}
        \pause{}
        where
        \begin{equation*}
            \underset{\scC\leftarrow\scB}{P}
            =
            \begin{bmatrix}
                [\bfb_1]_{\scB} &
                [\bfb_2]_{\scB} &
                \cdots &
                [\bfb_n]_{\scB}
            \end{bmatrix}
            .
        \end{equation*}
    \end{block}
    \pause{}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{change-of-basis-02.png}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{A digression}
%
%    \cake{} Show that for any basis $\scB$ of $V$, 
%    \begin{equation*}
%        [\bfzero]_{\scB}
%        =
%        \begin{bmatrix}
%            0 \\
%            0 \\
%            \vdots \\
%            0
%        \end{bmatrix}
%    \end{equation*}
%    
%    \pause{}
%
%    \think{}  Show that a 
%    $\{\bfu_{1}, \dots, \bfu_{p}\}$ 
%    in $V$ is linearly independent if and only if the set of coordinate vectors
%    $\{[\bfu_{1}]_{\scB}, \dots, [\bfu_{p}]_{\scB}\}$ 
%    is linearly independent in $\dsR^n$.
%\end{frame}

\begin{frame}
    \frametitle{The other way}

    The matrix 
    \begin{equation*}
        \underset{\scC\leftarrow\scB}{P}
        =
        \begin{bmatrix}
            [\bfb_1]_{\scB} &
            [\bfb_2]_{\scB} &
            \cdots &
            [\bfb_n]_{\scB}
        \end{bmatrix}
        .
    \end{equation*}
    is invertible because its columns are linearly independent.

    \pause{}

    Thus
    \begin{equation*}
        [\bfx]_{\scC}
        =
        \underset{\scC\leftarrow\scB}{P}
        \cdot
        [\bfx]_{\scB}
        ,
    \end{equation*}
    implies
    \begin{equation*}
        \left(\underset{\scC\leftarrow\scB}{P}\right)^{-1}
        \cdot
        [\bfx]_{\scC}
        =
        [\bfx]_{\scB}
        .
    \end{equation*}
    In other words,
    \begin{equation*}
        \underset{\scB\leftarrow \scC}{P}
        =
        \left(\underset{\scC\leftarrow\scB}{P}\right)^{-1}
        .
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{Change of Basis in $\dsR^{n}$}
%
%    If 
%    $\scB = \{\bfb_1,\dots, \bfb_n\}$ 
%    and
%    $\scC = \{\bfc_1,\dots, \bfc_n\}$ 
%    are bases of $\dsR^{n}$,
%    and 
%    $\scE = \{\bfe_1,\dots, \bfe_n\}$
%    is the standard basis,
%    then
%    \begin{equation*}
%        \underset{\scE \leftarrow \scB}{P}
%        =
%        \begin{bmatrix}
%            \bfb_1,\dots, \bfb_n
%        \end{bmatrix}
%        ,
%        \qquad
%        \underset{\scE \leftarrow \scC}{P}
%        =
%        \begin{bmatrix}
%            \bfc_1,\dots, \bfc_n
%        \end{bmatrix}
%        .
%    \end{equation*}
%
%    \pause{}
%
%    Thus
%    \begin{equation*}
%        \underset{\scC \leftarrow \scB}{P}
%        =
%        \underset{\scC \leftarrow \scE}{P}
%        \cdot
%        \underset{\scE \leftarrow \scB}{P}
%        =
%        \left(\underset{\scE \leftarrow \scC}{P}\right)^{-1}
%        \cdot
%        \underset{\scE \leftarrow \scB}{P}
%        .
%    \end{equation*}
%
%    \pause{}
%
%    To compute
%    $\underset{\scC \leftarrow \scB}{P}$
%    by hand, use
%    \begin{equation*}
%        \begin{bmatrix}
%            \underset{\scE \leftarrow \scC}{P}
%            &
%            \underset{\scE \leftarrow \scB}{P}
%        \end{bmatrix}
%        \sim
%        \begin{bmatrix}
%            I 
%            &
%            \underset{\scC \leftarrow \scB}{P}
%        \end{bmatrix}
%    \end{equation*}
%\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Let 
    \begin{equation*}
        \scB =
        \left\{
            \begin{bmatrix}
                -9 \\ 1
            \end{bmatrix}
            ,
            \begin{bmatrix}
                -5 \\ -1
            \end{bmatrix}
        \right\}
        ,
        \qquad
        \scC =
        \left\{
            \begin{bmatrix}
                1 \\ -4
            \end{bmatrix}
            ,
            \begin{bmatrix}
                3 \\ -5
            \end{bmatrix}
        \right\}
        .
    \end{equation*}

    \cake{} What are
    \begin{equation*}
        \underset{\scE \leftarrow \scB}{P}
        ,
        \quad
        \underset{\scE \leftarrow \scC}{P}
        ,
        \quad
        \underset{\scC \leftarrow \scB}{P}
        .
    \end{equation*}

    %\pause{}

    %\think{} Compute
    %$
    %\underset{\scC \leftarrow \scB}{P}
    %$
    %using
    %\begin{equation*}
    %    \begin{bmatrix}
    %        \underset{\scE \leftarrow \scC}{P}
    %        &
    %        \underset{\scE \leftarrow \scB}{P}
    %    \end{bmatrix}
    %    \sim
    %    \begin{bmatrix}
    %        I 
    %        &
    %        \underset{\scC \leftarrow \scB}{P}
    %    \end{bmatrix}
    %\end{equation*}
\end{frame}

\end{document}
