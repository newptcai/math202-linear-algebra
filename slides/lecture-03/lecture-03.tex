\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 03 --- Matrix Equations, Solution Sets}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{1.4 The Matrix Equations $A \bfx = \bfb$}

\subsection{Matrix Equations}

\begin{frame}{Matrix vector product}

    If $A$ is a $m \times n$ matrix, with columns
    $\bfa_1,\dots, \bfa_n$, and $\bfx \in \dsR^n$, then
    the \alert{product of $A$ and $\bfx$} is defined by
    \begin{equation*}
        A \bfx = 
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \cdots & \bfa_n
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ \vdots \\ x_n
        \end{bmatrix}
        =
        x_1 \bfa_1
        +
        x_2 \bfa_2
        +
        \dots
        +
        x_n \bfa_n
        .
    \end{equation*}

    In other words, $A \bfx$ is the \question{}
    of $\bfa_1, \dots, \bfa_n$ with weight $x_1, \dots, x_n$.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 1}

    \begin{equation*}
        \begin{bmatrix}
            1 & 2 & -1 \\
            0 & -5 & 3 \\
        \end{bmatrix}
        \begin{bmatrix}
            4 \\ 3 \\ 7
        \end{bmatrix}
        =
        \begin{bmatrix}
            3 \\ 6
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{Identity matrix}

    Compute the following
    \begin{equation*}
        \begin{bmatrix}
            1 & 0 & 0 \\ 
            0 & 1 & 0 \\ 
            0 & 0 & 1
        \end{bmatrix}
        \begin{bmatrix}
            r \\ s \\ t
        \end{bmatrix}
        =
        \text{\question{}}
    \end{equation*}

    \pause{}

    The $n \times n$ matrix with $1$ on the diagonal and $0$ everywhere,
    denoted by $I_n$,
    is called the \alert{identity matrix}.
\end{frame}

\begin{frame}[t]
    \frametitle{Linear system as matrix equations}
    
    Consider the matrix equation
    \begin{equation*}
        \begin{bmatrix}
            1 & 2 & -1 \\
            0 & -5 & 3 \\
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ x_2 \\ x_3
        \end{bmatrix}
        =
        \begin{bmatrix}
            4 \\ 1
        \end{bmatrix}
    \end{equation*}
    What is the equivalent linear system?
\end{frame}

\begin{frame}
    \frametitle{Three ways to write linear systems}
    
    \begin{block}{Theorem 3}
    If $A$ is a $m \times n$ matrix, with columns
    $\bfa_1,\dots, \bfa_n$, and $\bfb \in \dsR^m$, then
    the \emph{matrix equation}
    \begin{equation*}
        A \bfx = \bfb
    \end{equation*}
    \pause{}
    has the same solution set as the \emph{vector equation}
    \begin{equation*}
        x_1 \bfa_1
        +
        x_2 \bfa_2
        +
        \dots
        +
        x_n \bfa_n
        = 
        \bfb
    \end{equation*}
    \pause{}
    and the \emph{linear system} with the \emph{augmented matrix}
    \begin{equation*}
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \cdots & \bfa_n & \bfb
        \end{bmatrix}
    \end{equation*}
    \end{block}
\end{frame}

\subsection{Existence of Solutions}

\begin{frame}[t]
    \frametitle{Example 3}
    
    \think{} Is the following equation \emph{consistent} for all $b_1, b_2, b_3$?
    \begin{equation*}
        \begin{bmatrix}
            1 & 3 & 4 \\
            -4 & 2 & -6 \\
            -3 & -2 & -7 \\
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ x_2 \\ x_3
        \end{bmatrix}
        =
        \begin{bmatrix}
            b_1 \\ b_2 \\ b_3
        \end{bmatrix}
    \end{equation*}

    \pause{}

    Hint: 
    \begin{equation*}
        \begin{bmatrix}
            1 & 3 & 4 & b_1 \\
            -4 & 2 & -6 & b_2 \\
            -3 & -2 & -7 & b_3 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & 3 & 4 & b_1 \\
            0 & 14 & 10 & b_2 + 4 b_{1} \\
            0 & 0 & 0 & b_{3} + 3 b_{1} - \frac{1}{2} (b_{2} + 4 b_{1}) \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{When is a linear system consistent}
    
    \begin{block}{Theorem 4}
        If $A$ is a $m \times n$ matrix, with columns
        $\bfa_1,\dots, \bfa_n$, and $\bfx \in \dsR^n$, then
        the followings are equivalent
        \begin{enumerate}[<+->]
        \item[a.] For each $\bfb$, the equation $A \bfx = \bfb$ has a solution, i.e., it is
            \emph{consistent}.
        \item[b.] For each $\bfb$, $\bfb$ is a \emph{linear combination} of $\bfa_1, \dots, \bfa_n$.
        \item[c.] For each $\bfb$, $\bfb$ is in $\Span{\bfa_1, \dots, \bfa_n}$.
        \item[d.] $A$ has a pivot position in every row.
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}[t]
    \frametitle{Property of matrix vector product}
    
    \begin{block}{Theorem 5}
        If $A$ is $m \times n$ matrix, $\bfu, \bfv \in \dsR^n$, $c \in \dsR$, then
        \begin{enumerate}[a.]
            \item $A (\bfu + \bfv) = A \bfu + A \bfv$
            \item $A (c \bfu) = c A \bfu$
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}[c]
    \frametitle{Think, Pair and Share}
    
    In the next few minutes \clock{}, you will be given an exercise.
    Then you will

    \pause{}

    \begin{itemize}[<+->]
        \item Think -- Work on the problem by yourself.
        \item Pair -- Compare your solution with another student sitting besides you.
        \item Share -- A student will be chosen to present the solution to the class.
    \end{itemize}

    \onslide<+->{\medal{} If you represent a correct answer, you get 0.1 bonus point for your
    final grade.}
\end{frame}

\begin{frame}
    \frametitle{Think, Pair and Share \sweat{}}

    Can you find $\bfa_1, \bfa_2, \bfa_3 \in \dsR^{4}$ 
    such that $\Span{\bfa_1, \bfa_2, \bfa_3} = \dsR^{4}$? 

    \pause{}

    \begin{block}{\hint{} Theorem 4}
        If $A$ is a $m \times n$ matrix, with columns
        $\bfa_1,\dots, \bfa_n$, and $\bfx \in \dsR^n$, then
        the followings are equivalent
        \begin{enumerate}
            \item[c.] For each $\bfb$, $\bfb$ is in $\Span{\bfa_1, \dots, \bfa_n}$.
            \item[d.] $A$ has a pivot position in every row.
        \end{enumerate}
    \end{block}
    \medal{} What about $n$ vectors in $\dsR^{m}$ with $n < m$?
\end{frame}

\section{1.5 Solution Sets of Linear Systems}

\subsection{Homogeneous Linear Systems}

\begin{frame}
    \frametitle{Homogeneous linear systems}

    A linear system of the form $A \bfx = \bfzero$ is called \alert{homogeneous}.

    \pause{}
    
    The following homogeneous system
    \begin{equation*}
        \systeme{3 x_1 + 5 x_2 - 4 x_3 = 0,
            -3 x_1 -2 x_2 + 4 x_3 = 0,
            6 x_1 + x_2 - 8 x_3 = 0}
    \end{equation*}

    \pause{}
    
    A \alert{trivial} solution for this system is
    \begin{equation*}
        \left\{
            \begin{aligned}
                & x_1 = 0 \\
                & x_2 = 0 \\
                & x_3 = 0
            \end{aligned}
        \right.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Nontrivial solutions}

    A homogeneous system \emph{always} has a trivial solution $\bfzero$, i.e., 
    it is \emph{always} consistent.

    \pause{}

    By the following, 
    it has nontrivial solutions
    if and only if there is/are \question{} free variable.

    \begin{block}{Theorem 2}
        A consistent linear system has
        \begin{itemize}
            \item infinite solutions if it has \emph{at least one} free variables, or
            \item a unique solutions if it has \emph{no} free variables.
        \end{itemize}
    \end{block}
\end{frame}

%\begin{frame}[t]
%    \frametitle{Example 1}
%    Solve the following homogeneous system
%    \begin{equation*}
%        \systeme{3 x_1 + 5 x_2 - 4 x_3 = 0,
%            -3 x_1 -2 x_2 + 4 x_3 = 0,
%            6 x_1 + x_2 - 8 x_3 = 0}
%    \end{equation*}
%    using
%    \begin{equation*}
%        \begin{bmatrix}
%            3 & 5 & -4 & 0 \\
%            -3 & -2 & 4 & 0 \\
%            6 & 1 & -8 & 0\\
%        \end{bmatrix}
%        \sim
%        \begin{bmatrix}
%            1 & 0 & -\frac{4}{3} & 0 \\
%            0 & 1 & 0 & 0 \\
%            0 & 0 & 0 & 0 \\
%        \end{bmatrix}
%    \end{equation*}
%\end{frame}

\begin{frame}
    \frametitle{Example 2}
    
    Show that the homogeneous system
    \begin{equation*}
        10 x_1 - 3 x_2 - 2 x_3 = 0
    \end{equation*}
    has a solution set
    \begin{equation*}
        \bfx =  
        x_{2}
        \begin{bmatrix}
            0.3 \\ 1 \\ 0
        \end{bmatrix}
        +
        x_{3}
        \begin{bmatrix}
            0.2 \\ 0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Parametric vector form}
    
    The solution set of Example 2 can be written as
    \begin{equation*}
        \bfx = s \bfu + t \bfv,
        \qquad
        \text{where }
        s, t \in \dsR,
        \quad
        \bfu = 
        \begin{bmatrix}
            0.3 \\ 1 \\ 0
        \end{bmatrix}
        ,
        \quad
        \bfv = 
        \begin{bmatrix}
            0.2 \\ 0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
    This is called a solution in \alert{parametric vector form}.

    In other words, the solution is the \question{} of $\bfu$ and $\bfv$.
\end{frame}

\begin{frame}
    \frametitle{Solution sets}

    \emoji{bulb} The solution set of a homogeneous system is of the form
    $\Span{\bfv_{1}, \dots, \bfv_{p}}$. 
    This is called the \emph{parametric vector form} of a solution set.

    \small

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{span-figure.png}
        \caption*{The spans of vectors}%
    \end{figure}
\end{frame}

\subsection{Nonhomogeneous Linear Systems}

\begin{frame}
    \frametitle{Example 3}
    
    The solutions of $A \bfx = \bfb$ with
    \begin{equation}\label{eq:inhom}
        A =
        \begin{bmatrix}
            3 & 5 & -4 \\ 
            -3 & -2 & 4 \\ 
            6 & 1 & -8 \\
        \end{bmatrix}
        ,
        \qquad
        b
        =
        \begin{bmatrix}
            7 \\ -1 \\ -4
        \end{bmatrix}
    \end{equation}
    has the solution set
    \begin{equation*}
        \bfx = \bfp + t \bfv,
        \qquad
        \text{where }
        \bfp = 
        \begin{bmatrix}
            -1  \\ 2 \\ 0
        \end{bmatrix}
        ,
        \quad
        \bfv = 
        \begin{bmatrix}
            \frac{4}{3}  \\ 0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}

    \pause{}

    \hint{} Note that 
    \begin{enumerate}
        \item $t \bfv$ is the solution set of $A \bfx = \bfzero$;
        \item $A \bfp = \bfb$, i.e., $\bfp$ is a solution of \eqref{eq:inhom}.
    \end{enumerate}
\end{frame}

%\begin{frame}
%    \frametitle{Geometric Interpretation}
%
%    The solution can be seen as \emph{the line through p parallel to v}.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{nonhomogeneous-solutions.png}
%        \caption*{The solution sets of $A \bfx = \bfzero$ and $A \bfx = \bfb$}%
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Solution sets}
    \begin{block}{Theorem 6}
    The solution set of a nonhomogeneous system $A \bfx = \bfb$ 
    is of the form $\bfp + t \bfv$,
    where $\bfp$ is one particular solution,
    and $\bfv$ is any solution of $A \bfx = \bfzero$.
    \end{block}
\end{frame}
\end{document}
