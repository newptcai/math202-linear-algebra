# # Lecture 24
using LinearAlgebra, RowEchelon, SymPy, Plots, LaTeXStrings

## For making slides
using Latexify
copy_to_clipboard(true)

# # Projection
plot([0,3],[0, 4],arrow=true,linewidth=4,label="",aspect_ratio=1, size=(600,800))
plot!([0,3],[0, 0],arrow=true,linewidth=4,label="",aspect_ratio=1)
plot!([3,3],[0, 4],arrow=true,linewidth=4,label="",aspect_ratio=1)
plot!([0,1],[0, 0],arrow=true,linewidth=4,label="",aspect_ratio=1)
annotate!(1.2,2.2,text(L"\mathbf{y} = \mathbf{z} + \mathbf{\hat{y}}",20))
annotate!(1.7,0.2,text(L"\mathbf{\hat{y}}",20))
annotate!(0.5,0.2,text(L"\mathbf{u}",20))
annotate!(2.8,2,text(L"\mathbf{z}",20))