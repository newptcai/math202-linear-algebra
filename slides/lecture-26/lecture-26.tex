\input{../meta.tex}

\title{Lecture 26 --- Least-squares Problems, Applications to Linear Models}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{6.5 Least-squares Problems}

\begin{frame}
    \frametitle{Least-squares solutions}

    A \alert{least-squares solution} of $A \bfx = \bfb$ is a vector $\hat{\bfx}$ such
    that
    \begin{equation*}
        \norm{\bfb - A \hat{\bfx}}
        \le
        \norm{\bfb - A \bfx}
        ,
        \qquad
        \text{for all } \bfx.
    \end{equation*}
    This impels that
    \begin{equation*}
        A \hat{\bfx} = \proj_{\colspace A} \bfb = \hat{\bfb}
    \end{equation*}
    
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.5-fig-1.png}
            \end{figure}
        \end{column}
        \begin{column}{0.4\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Find least-squares solutions}
    
    \begin{block}{Theorem 13}
    The set of least-squares solutions of $A \bfx = \bfb$ 
    coincides with the nonempty set of solutions of 
    $A^T A \bfx =A^T \bfb$.
    \end{block}

    \pause{}

    \cake{} Why is of $A^{T} A \bfx = A^{T} \bfb$ always consistent?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{6.5-fig-2.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    Find the least-squares solutions for
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            4 & 0 \\
            0 & 2 \\
            1 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfb
        =
        \begin{bmatrix}
            2 \\ 0 \\ 11
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Unique least-squares solutions}
    
    \begin{block}{Theorem 13}
    Let $A$ be a $m \times n$ matrix. The following statements are equivalent:
    \begin{enumerate}[a.]
        \item The equation $A \bfx = \bfb$ has a unique least-squares solution for each
            $\bfb \in \dsR^m$.
        \item The columns of $A$ are linearly independent.
        \item The matrix $A^T A$ is invertible.
    \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Least-squares errors}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The distance from $\bfb$ to $A \hat{\bfx}$ is called the
            \alert{least-squares error}.

            The error in Example 1 is $\sqrt{84}$.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.5-fig-3.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 4}
    
    What are the least-squares solutions for
    \begin{equation*}
        A = 
        \begin{bmatrix}
            1 & -6 \\
            1 & -2 \\
            1 & 1 \\
            1 & 7 \\
        \end{bmatrix}
        ,
        \qquad
        b =
        \begin{bmatrix}
            -1 \\ 2 \\ 1 \\ 6
        \end{bmatrix}
    \end{equation*}
    \hint{} The columns of $A$ are orthogonal.
\end{frame}

\begin{frame}
    \frametitle{Using QR factorization}
    
    \begin{block}{Theorem 15}
        Let $A$ be a matrix with linearly independent columns and let $A = QR$ be a
        QR factorization.
        Then for each $\bfb$ there is a unique least-squares solution
        \begin{equation*}
            \hat{\bfx} = R^{-1} Q^T \bfb
            .
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}

    \begin{block}{Theorem 13}
    Let $A$ be $m \times n$. The following statements are equivalent:
    \begin{enumerate}
        \item[b.] The columns of $A$ are linearly independent.
        \item[c.] The matrix $A^T A$ is invertible.
    \end{enumerate}
    \end{block}

    Can you show c \Rightarrow b?
    
    \hint{} Assume that the columns of $A$ are not linearly independent. Get a
    contradiction.
\end{frame}

\section{6.6 Applications to Linear Models}

\begin{frame}
    \frametitle{Because of Machine Learning}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{machine_learning_2x.png}
        \caption*{\url{https://xkcd.com/1838/}}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Fitting a line to experimental data}

    Give experimental data $(x_{1}, y_{1}), \dots (x_{n}, y_{n})$,
    how to find a line which \emph{best}
    fit the data?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{linear-regression.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Least-squares lines}

    The \alert{least-squares line} is the line $y = \beta_0 + \beta_1 x$ which
    minimizes \alert{the squares of the residues}.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{6.6-fig-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Finding the least-squares line}
    
    Let
    \begin{equation*}
        X =
        \begin{bmatrix}
            1 & x_{1} \\
            1 & x_{2} \\
            \vdots & \vdots \\
            1 & x_{n} \\
        \end{bmatrix}
        ,
        \quad
        \boldsymbol{\beta}
        =
        \begin{bmatrix}
            \beta_0 \\ \beta_1
        \end{bmatrix}
        ,
        \quad
        \bfy =
        \begin{bmatrix}
            y_{1} \\
            y_{2} \\
            \vdots \\
            y_{n} \\
        \end{bmatrix}
    \end{equation*}

    Then finding the \emph{least-squares solutions} of $X \boldsymbol{\beta} = \bfy$
    is the same as finding the \emph{least-squares line}.
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    Finding the least-square line for the data points $(2,1), (5,2), (7,3), (8,3)$.

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{6.6-example-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The General Linear Model}
    
    In general, a linear model is of the form $X \bfb = \bfy$,
    where $X$ can have various forms.

    The aim is to minimize the \emph{length} of the residue vector $\boldsymbol{\epsilon}$ which
    satisfies
    \begin{equation*}
        X \bfb = \bfy + \boldsymbol{\epsilon}.
    \end{equation*}

    This amounts to find the \emph{least-squares solutions} for $X \bfb = \bfy$ by
    finding $\hat{\boldsymbol{\beta}}$, the exact solution of
    \begin{equation*}
        X^{T} X \boldsymbol{\beta} = X^{T} \bfy.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    
    Given data $(x_{1}, y_{1}), \dots, (x_{n}, y_{n})$, how to find the
    \emph{least-squares fit} of the data by
    \begin{equation*}
        y = \beta_{0} + \beta_{1} x + \beta_{2} x^{2}
    \end{equation*}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.6-fig-4.png}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{Example 3}
%    
%    Given data $(x_{1}, y_{1}), \dots, (x_{n}, y_{n})$, how to find the
%    \emph{least-squares fit} of the data by
%    \begin{equation*}
%        y = \beta_{0} + \beta_{1} x + \beta_{2} x^{2} + \beta_{3} x^{3}
%    \end{equation*}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%            \begin{figure}[htpb]
%                \centering
%                \includegraphics[width=\linewidth]{6.6-fig-5.png}
%            \end{figure}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            
%        \end{column}
%    \end{columns}
%\end{frame}

%\begin{frame}
%    \frametitle{Multiple Regression}
%    
%    Given data $(u_{1}, v_{1}, y_{1}), \dots, (u_{n}, v_{n}, y_{n})$, how to find the
%    \emph{least-squares fit} of the data by
%    \begin{equation*}
%        y = \beta_{0} + \beta_{1} u + \beta_{2} v
%    \end{equation*}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%            \begin{figure}[htpb]
%                \centering
%                \includegraphics[width=\linewidth]{6.6-fig-6.png}
%            \end{figure}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            
%        \end{column}
%    \end{columns}
%\end{frame}

%\begin{frame}
%    \frametitle{One Last Exercise}
%    
%    \think{} Given the data $(1, 7.9), (2, 5.4), (3, -0.9)$,
%    write down the system of linear equations whose solution will give the
%    least-squares fit of the data in the form of
%    \begin{equation*}
%        y = \beta_{1} \cos(x) + \beta_{2} \sin(x)
%    \end{equation*}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%            \begin{figure}[htpb]
%                \centering
%                \includegraphics[width=\linewidth]{6.6-exercise.png}
%            \end{figure}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            
%        \end{column}
%    \end{columns}
%\end{frame}

\begin{frame}[standout]
    The end! \emoji{pray}
    \vspace{1em}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{fun.jpg}
    \end{figure}
\end{frame}

\end{document}
