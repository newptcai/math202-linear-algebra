\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 02 --- Solution Sets, Vector Equations}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{1.2 Row Reduction and Echelon Forms}%

\subsection{Solutions of Linear Systems}%

\begin{frame}
    \frametitle{Get the solutions}
    
    Suppose a linear system whose \emph{augmented matrix} 
    is in \emph{reduced echelon form}
    \begin{equation*}
            \begin{bmatrix}
                1 & 0 & -5 & 1 \\
                0 & 1 & 1 & 4 \\
                0 & 0 & 0 & 0 \\
            \end{bmatrix}
        \hspace{2cm}
        \pause{}
        \systeme{
            x_1 - 5 x_3 = 1,
            x_2 + x_3 = 4,
            0 = 0
        }
    \end{equation*}

    We call $x_1$ and $x_2$ the \alert{pivot variables}, 
    and $x_3$ is the \alert{free variable}.

    \cake{} What is the solution set?
    \begin{equation*}
        \left\{
            \begin{aligned}
        & x_1 =  \text{\question}\\
        & x_2 =  \text{\question}\\
        & x_3 \text{ is free}
            \end{aligned}
        \right.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Find the solution set of
    \begin{equation*}
        \begin{bmatrix}
            1 & 6 & 2 & -5 & -2 & 4 \\
            0 & 0 & 2 & -8 & -1 & 3 \\
            0 & 0 & 0 & 0 & 1 & 7 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{Back-Substitution}
%    
%    Another way to solve 
%    \begin{equation*}
%        \systeme{
%            x_1 - 7x_2 + 2 x_3 -5 x_4 + 8 x_5 = 10,
%            x_2 -3 x_3 +3 x_4 + x_{5} = -5,
%            x_4 - x_5 = 4
%        }
%    \end{equation*}
%    is \alert{back-substitution}.
%\end{frame}

\subsection{Existence and Uniqueness Questions}

\begin{frame}
    \frametitle{Consistent? Unique?}
    
    Is the following linear system (augmented matrix) \emph{consistent}?
    If it is consistent, is the solution unique?
    \begin{equation*}
        \begin{bmatrix}
            3 & -9 & 12 & -9 & 6 & 15 \\
            0 & 2 & -4 & 4 & 2 & -6 \\
            0 & 0 & 0 & 0 & 1 & 4\\
        \end{bmatrix}
    \end{equation*}

    \pause{}
    
    \cake{} What about 
    \begin{equation*}
        \begin{bmatrix}
            3 & -9 & 12 & -9 & 6 & 15 \\
            0 & 2 & -4 & 4 & 2 & -6 \\
            0 & 0 & 0 & 0 & 0 & 4\\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Existence and uniqueness theorem}
    
    \begin{block}{Theorem 2}
        A linear system is \emph{consistent} 
        if its echelon form has no row of the form
        \begin{equation*}
            \begin{bmatrix}
            0 & 0  &0  & \cdots & 0  & b
            \end{bmatrix}
        \qquad \text{where $b \ne 0$}.
        \end{equation*}

        \pause{}

        A consistent linear system has
        \begin{itemize}
            \item infinite solutions if it has \emph{at least one} free variables, or
            \item a unique solutions if it has \emph{no} free variables.
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{A simple rule \think{}}
    
    Can a $2 \times 4$ augmented matrix like the one below have a unique solution?
    \begin{equation*}
        \left[
            \begin{array}{cccccc}
                1 & 3 & 0 & 3 \\
                0 & 2 & 2 & 1 \\
            \end{array}
        \right]
    \end{equation*}

    \pause{}

    Can a $m \times n$ augmented matrix with $n - 1 > m$
    have a unique solution?
\end{frame}

\section{1.3 Vector Equations}

\subsection{Vectors}

\begin{frame}[c]
    \frametitle{Vectors in $\dsR^{2}$}

    A matrix with only one column is a \alert{column vector/vector}.
    Examples of vectors ---
    \begin{equation*}
        \boldsymbol{u}
        =
        \begin{bmatrix}
        3 \\
        -1
        \end{bmatrix}
        ,
        \qquad
        \boldsymbol{v}
        =
        \begin{bmatrix}
        0.2 \\
        0.3
        \end{bmatrix}
        ,
        \qquad
        \boldsymbol{w}
        =
        \begin{bmatrix}
        w_{1} \\
        w_{2}
        \end{bmatrix}
    \end{equation*}
    where $w_1,w_2$ are real numbers.

    The set of all vectors with two entries is $\dsR^{2}$,
    where $\dsR$ denotes all real numbers.
\end{frame}

\begin{frame}
    \frametitle{Vector arithmetic}

    Equality
    \begin{equation*}
        \begin{bmatrix}
        3 \\
        -1 \\
        \end{bmatrix}
        \ne
        \begin{bmatrix}
        -1 \\
        3 \\
        \end{bmatrix}
        ,
        \qquad
        \begin{bmatrix}
        3 \\
        -1 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
        3 \\
        -1 \\
        \end{bmatrix}
        .
    \end{equation*}
    Addition
    \begin{equation*}
        \begin{bmatrix}
        1 \\
        -2 \\
        \end{bmatrix}
        +
        \begin{bmatrix}
        2 \\
        5 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
        1 + 2\\
        -2 + 5\\
        \end{bmatrix}
        =
        \begin{bmatrix}
        3 \\
        3 \\
        \end{bmatrix}
        .
    \end{equation*}
    Subtraction
    \begin{equation*}
        \begin{bmatrix}
        1 \\
        -2 \\
        \end{bmatrix}
        -
        \begin{bmatrix}
        2 \\
        5 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
        1 - 2\\
        -2 - 5\\
        \end{bmatrix}
        =
        \begin{bmatrix}
        -1 \\
        -7 \\
        \end{bmatrix}
        .
    \end{equation*}
    Scalar multiple
    \begin{equation*}
        c
        \begin{bmatrix}
        1 \\
        -2 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
        c \cdot 1 \\
        c \cdot -2 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
        c \\
        -2 c \\
        \end{bmatrix}
    \end{equation*}
    where $c$ is a real number.
\end{frame}

\subsection{Geometric Descriptions}

\begin{frame}[c]
    \frametitle{Points on a plane}
    
    We can think of $\dsR^{2}$ as the set of points on a plane.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{r2-01.png}
        \caption*{Pictures of some vectors}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Vector addition}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{r2-add.png}
        \caption*{The parallelogram rule of vector addition}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Scalar multiple}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{r2-multiple.png}
        \caption*{The scalar multiples in $\dsR^{2}$}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Vectors in $\dsR^{3}$}
    
    $\dsR^{3}$ is set of vectors with $3$ entries.
    We can think of it as the set of points in the $3$-dimensional space.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{r3-multiple.png}
        \caption*{The scalar multiples in $\dsR^{3}$}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Vectors in $\dsR^{n}$}
    
    $\dsR^{n}$ is set of vectors with $n$ entries, e.g.,
    \begin{equation*}
        \boldsymbol{u}
        =
        \begin{bmatrix}
            u_{1} \\
            u_{2} \\
            \vdots \\
            u_{n} \\
        \end{bmatrix}
    \end{equation*}
    where $u_{1}, u_{2}, \dots, u_{n}$ are real numbers.

    The zero vector $\boldsymbol{0}$ is the vector with all entries being $0$.
\end{frame}

\begin{frame}
    \frametitle{Algebra of $\boldsymbol{\dsR^{n}}$}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{rn-algebra.png}
    \end{figure}
\end{frame}

\subsection{Linear Combinations}%

\begin{frame}
    \frametitle{Linear combinations}

    The \alert{linear combination} of vectors $\bfv_{1},\dots, \bfv_{p}$
    with weight $c_1, \dots, c_p$ is the vector
    \begin{equation*}
        \boldsymbol{y} = 
        c_1 \boldsymbol{v}_1
        +
        c_2 \boldsymbol{v}_2
        +
        \dots
        +
        c_p \boldsymbol{v}_p
        .
    \end{equation*}

    Some linear combinations of $\bfv_1,\bfv_2$ are
    \begin{equation*}
        \sqrt{3} \bfv_1+ \bfv_2
        ,
        \qquad
        \frac{1}{2} \bfv_1
        ,
        \qquad
        \boldsymbol{0}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Vector equations}

    Let
    \begin{equation*}
        \bfa_1
        =
        \begin{bmatrix}
            1, \\ -2 \\ 5
        \end{bmatrix}
        ,
        \qquad
        \bfa_2
        =
        \begin{bmatrix}
            2, \\ 5 \\ 6
        \end{bmatrix}
        ,
        \qquad
        \bfb
        =
        \begin{bmatrix}
            7, \\ 4 \\ -3
        \end{bmatrix}
    \end{equation*}

    Can $\bfb$ be an \emph{linear combination} of $\bfa_{1}, \bfa_{2}$?
    Can we find $x_1$ and $x_2$ such that 
    \begin{equation*}
        x_1 \bfa_1 + x_2 \bfa_2 = \bfb
    \end{equation*}

    \pause{}

    This is the same as solving
    \begin{equation*}
        \begin{bmatrix}
            \bfa_1 & \bfa_2  & \bfb
        \end{bmatrix}    
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Linear combinations and linear systems}

    The equation
    \begin{equation*}
        x_1 \bfa_1 + x_2 \bfa_2 + \cdots + x_n \bfa_n = \bfb
    \end{equation*}
    has the same solution set as the linear system
    \begin{equation*}
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \cdots & \bfa_n & \bfb
        \end{bmatrix}    
    \end{equation*}

    In other words, $\bfb$ is a linear combination 
    of $\bfa_1, \dots, \bfa_n$ if the system has a solution.
\end{frame}

\subsection{Spans}

\begin{frame}[c]
    \frametitle{Spans}
    Let $\veclist[p]{v}$ be vectors in $\dsR^{n}$.

    The set of \emph{all} linear combinations, denoted by
    \begin{equation*}
        \Span{\veclist[p]{v}},
    \end{equation*}
    is called the subset of $\dsR^{n}$ \alert{spanned} by $\veclist[p]{v}$.

    \cake{} What are the \emph{spans} of $\bfzero$?
\end{frame}

\begin{frame}[c]
    \frametitle{Geometric Description}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{span-figure.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 6}
    
    Let
    \begin{equation*}
        \bfa_{1}
        =
        \begin{bmatrix}
            1 \\ -2 \\ 3
        \end{bmatrix}
        ,
        \quad
        \bfa_{2}
        =
        \begin{bmatrix}
            5 \\ -13 \\ -3
        \end{bmatrix}
        ,
        \quad
        \bfb
        =
        \begin{bmatrix}
            -3 \\ 8 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
    Is $\bfb$ in $\Span{\bfa_1, \bfa_2}$?
\end{frame}

\begin{frame}
    \frametitle{Exercise \think{}}
    
    Can $\bfb$ be a linear combination of $\bfv_1, \bfv_2, \bfv_3$?
    Is the solution unique?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{linear-combination-exercise.png}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Centre of mass}
%    
%    Let $\bfv_{1}, \dots, \bfv_{k}$ be points in points in $\dsR^{n}$.
%    Assume an object with mass $m_{j}$ is located at $\bfv_{j}$.
%    Let the total mass be
%    \begin{equation*}
%        m = m_{1} + \dots + m_{k}.
%    \end{equation*}
%    The \alert{centre of gravity (mass)} is
%    \begin{equation*}
%        \bar{\bfv} = \frac{1}{m}
%        (m_1 \bfv_1 + \dots + m_k \bfv_k).
%    \end{equation*}
%
%    When the \emoji{crescent-moon} orbits around the \emoji{earth-asia}, it actually orbits around 
%    \href{https://en.wikipedia.org/wiki/Center\_of\_mass\#/media/File:Orbit3.gif}{the
%        centre of mass} of the two bodies.
%\end{frame}

\end{document}
