# # Lecture 11
using LinearAlgebra, RowEchelon

## For making slides
using Latexify
copy_to_clipboard(true)

# ## Cramer's rule

## An experiment
I4 = Matrix{Int}(I(4)) # Identity matrix
A = rand(-1:3, (4,4)) # A random matrix
A*I4[:, 1] # A * e1
A
A*I4[:, 2] # A * e2
A
A*I4[:, 3] # A * e3
A
A*I4[:, 4] # A * e4
A