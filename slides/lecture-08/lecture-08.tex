\input{../meta.tex}

\title{Lecture 08 --- Characterizations of Invertible Matrices, Partitioned Matrices}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{2.3 Characterizations of Invertible Matrices}

\subsection{The Invertible Matrix Theorem}

\begin{frame}
    \frametitle{The Invertible Matrix Theorem}

    \begin{block}{Theorem 8}
    \scriptsize{}
    Let $A$ be $n \times n$.
    The following are equivalent.
    \vspace{-1em}
    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
        \item[c.] $A$ has $n$ pivot positions.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[e.] The columns of $A$ form a linearly independent set.
        \item[f.] The linear transformation $x \mapsto A \bfx$ is one-to-one.
        \item[g.] The equation $A \bfx = $ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[h.] The columns of $A$ spans $\dsR^n$.
        \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
        \item[j.] There is an $n \times n$ matrix C such that $CA = I_n$.
        \item[k.] There is an $n \times n$ matrix D such that $AD = I_n$.
        \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}[c]
    \frametitle{A road map of proof}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.38\linewidth]{invertible-matrix-01.png}
        \hfill
        \includegraphics[width=0.38\linewidth]{invertible-matrix-02.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{(j) \Rightarrow{} (d)}

    \think{} Prove (j) \Rightarrow{} (d).
    \begin{enumerate}
        \item[j.] There is an $n \times n$ matrix C such that $CA = I_n$.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
    \end{enumerate}

    \hint{} Assume that for some $\bfx \ne 0$,
    $A \bfx = 0$. Find a contradiction.
\end{frame}

\begin{frame}
    \frametitle{Proof (1)}
    
    \begin{block}{Theorem 8}
    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[j.] There is an $n \times n$ matrix C such that $CA = I_n$.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[c.] $A$ has $n$ pivot positions.
        \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
    \end{enumerate}
    \end{block}

    \begin{itemize}
        \item (a) \Rightarrow{} (j) \cake{}
        \item (j) \Rightarrow{} (d) \checkbutton{}
        \item (d) \Rightarrow{} (c) \cake{}
        \item (c) \Rightarrow{} (b) \cake{}
        \item (b) \Rightarrow{} (a) Theorem 7.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Proof (2)}

    \begin{block}{Theorem 8}
    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[k.] There is an $n \times n$ matrix D such that $AD = I_n$.
        \item[g.] The equation $A \bfx = $ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[c.] $A$ has $n$ pivot positions.
    \end{enumerate}
    \end{block}

    \begin{itemize}
        \item (a) \Rightarrow{} (k) \cake{}
        \item (k) \Rightarrow{} (g) \hint{} $A D \bfb = \bfb$.
        \item (g) \Rightarrow{} (c) By Theorem 4 (Ch.\ 1).
        \item (c) \Rightarrow{} (a) \checkbutton{}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Proof (3)}

    \begin{block}{Theorem 8}
    \begin{enumerate}
        \item[g.] The equation $A \bfx = $ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[h.] The columns of $A$ spans $\dsR^n$.
        \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
    \end{enumerate}
    \end{block}

    \begin{itemize}
        \item (g) \Leftrightarrow{} (h) By Theorem 4 (Ch.\ 1).
        \item (g) \Leftrightarrow{} (i) \cake{}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Proof (4)}
    
    \begin{block}{Theorem 8}
    \begin{enumerate}
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[e.] The columns of $A$ form a linearly independent set.
        \item[f.] The linear transformation $x \mapsto A \bfx$ is one-to-one.
    \end{enumerate}
    \end{block}

    \begin{itemize}
        \item (d) \Leftrightarrow{} (e) \cake{}
        \item (e) \Leftrightarrow{} (f) By Theorem 12 (Ch.\ 1)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Proof (5)}
    
    \begin{block}{Theorem 8}
    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}
    \end{block}

    \begin{itemize}
        \item (a) \Rightarrow{} (l) By Theorem 6 (Ch.\ 2)
        \item (l) \Rightarrow{} (a) \cake{}
    \end{itemize}

    \begin{block}{Theorem 6}
        \begin{enumerate}
            \item[c.] If $A$ is invertible, then so is $A^{T}$.
        \end{enumerate}
    \end{block}
\end{frame}

\subsection{Invertible Linear Transformations}%

\begin{frame}[c]
    \frametitle{Invertible}

    A linear transform 
    $T: \dsR^{n} \mapsto \dsR^{n}$ 
    is \alert{invertible} if there exists 
    $S: \dsR^{n} \mapsto \dsR^{n}$ 
    such that 
    \begin{equation*}
        \begin{aligned}
            & S(T(\bfx)) = \bfx, \qquad (\text{for all } \bfx \in \dsR^{n}) \\
            & T(S(\bfx)) = \bfx, \qquad (\text{for all } \bfx \in \dsR^{n}) \\
        \end{aligned}
    \end{equation*}

    $S$ is called the \alert{inverse} of $T$.

    \cake{} Why is $T$ onto?
\end{frame}

\begin{frame}
    \frametitle{Relation to invertible matrices}
    
    \begin{block}{Theorem 9}
        Let $T: \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation and
        let $A$ be its standard matrix.
        Then $T$ is \emph{invertible} if and only if $A$ is \emph{invertible}.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Think-Pair-Share}
    
    \think{} Prove that if a linear mapping $T:\dsR^{n} \mapsto \dsR^{n}$ is one-to-one, 
    then $T$ is invertible.

    \pause{}

    \hint{} Use the Invertible Matrix Theorem \pause{} and the following
    \begin{block}{Theorem 9}
        Let $T: \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation and
        let $A$ be its standard matrix.
        Then $T$ is \emph{invertible} if and only if $A$ is \emph{invertible}.
    \end{block}
    \pause{}
    \begin{block}{Theorem 12 (Ch.\ 1)}
        Let $T: \dsR^{m} \mapsto\dsR^{n}$ be a linear transformation, 
        and let $A$ be its standard matrix.

        Then $T$ is one-to-one if and only if the columns of $A$ are \emph{linearly
        independent}.
    \end{block}
\end{frame}

\section{2.4 Partitioned Matrices}%

\begin{frame}
    \frametitle{Partition a matrix into blocks}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{partition-example-01.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Multiplication}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{partition-example-02.png}
        \includegraphics[width=\linewidth]{partition-example-03.png}
    \end{figure}
    
    \emoji{fire} 
    Movition:
    \begin{itemize}
        \item Useful when the matrices are too lage for the memory of computers.
        \item Speed up computations.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Column-Row Expansion}

    \begin{block}{Theorem 10}
        \scriptsize{}

        If $A$ is $m \times n$ and $B$ is $n \times p$, then
        \begin{equation*}
            \begin{aligned}
            A B
            &
            =
            \begin{bmatrix}
                \mcol_1(A) & \mcol_2(A) & \cdots & \mcol_n(A)
            \end{bmatrix}
            \begin{bmatrix}
                \mrow_1(B) \\ \mrow_2(B) \\ \cdots \\ \mrow_n(B)
            \end{bmatrix}
            \\
            &
            =
            \mcol_1(A)
            \mrow_1(B)
            +
            \mcol_2(A)
            \mrow_2(B)
            +
            \cdots
            +
            \mcol_n(A)
            \mrow_n(B)
            .
            \end{aligned}
        \end{equation*}
    \end{block}

    \pause{}

    \think{} Verify this by compute the product in two ways
    \begin{equation*}
        \begin{bmatrix}
            -3 & 1 & 2 \\
            1 & -4 & 5 \\
        \end{bmatrix}
        \begin{bmatrix}
            a & b \\
            c & d \\
            e & f \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Inverses of Partitioned Matrices}
    
    The matrix
    \begin{equation*}
        A = 
        \begin{bmatrix}
            A_{11} & A_{12} \\
            0 & A_{22} \\
        \end{bmatrix}
    \end{equation*}
    is said to be \alert{block upper triangular}

    If $A$ is invertible, then
    \begin{equation*}
        A^{-1}
        =
        \begin{bmatrix}
            A_{11}^{-1} & -A_{11}^{-1} A_{12} A_{22}^{-1} \\
            0 & A_{22}^{-1} \\
        \end{bmatrix}
    \end{equation*}

    \cake{} Proof?
\end{frame}

\end{document}
