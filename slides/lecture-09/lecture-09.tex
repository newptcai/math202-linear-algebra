\input{../meta.tex}

\title{Lecture 09 --- Matrix Factorizations}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{2.5 Matrix Factorizations}

\subsection{The LU Factorization}

\begin{frame}[c]
    \frametitle{The LU factorization}
    
    If $A$ can be row reduced to echelon form \emph{without row interchanges}, 
    then it has an \alert{LU factorization}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{LU-01.png}
    \end{figure}

    The matrix $L$ is called a \alert{unit lower triangular matrix}.
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            3 & -7 & -2 & 2\\
            -3 & 5 & 1 & 0\\
            6 & -4 & 0 & -5\\
            -9 & 5 & -5 & 12\\
        \end{bmatrix}
        =
        \begin{bmatrix}
            1 & 0 & 0 & 0\\
            -1 & 1 & 0 & 0\\
            2 & -5 & 1 & 0\\
            -3 & 8 & 3 & 1\\
        \end{bmatrix}
        \begin{bmatrix}
            3 & -7 & -2 & 2\\
            0 & -2 & -1 & 2\\
            0 & 0 & -1 & 1\\
            0 & 0 & 0 & -1\\
        \end{bmatrix}
        =
        L U
    \end{equation*}

    How does this simplify solving $A \bfx = \bfb$?
\end{frame}

\begin{frame}
    \frametitle{The idea of the algorithm}
    
    Assume that there exists \emph{unit lower triangular elementary} matrices
    $E_1, \dots, E_p$ such that
    \begin{equation*}
        E_p \dots E_1 A = U
    \end{equation*}
    where $U$ is in echelon form.

    \only<1>{\cake{} Example ---
    \begin{equation*}
        \begin{gathered}
            A 
            =
            \left[
                \begin{array}{ccc}
                    1 & 1 & 1 \\
                    2 & 2 & 2 \\
                    3 & 3 & 3 \\
                \end{array}
            \right]
            ,
            \quad
            E_{1}
            =
            \left[
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    -2 & 1 & 0 \\
                    0 & 0 & 1 \\
                \end{array}
            \right]
            ,
            \quad
            E_{2} = 
            \left[
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    0 & 1 & 0 \\
                    -3 & 0 & 1 \\
                \end{array}
            \right]
            ,
            \\
            E_{1} A
            =
            \left[
                \phantom{
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    0 & 1 & 0 \\
                    1 & 0 & 1 \\
                \end{array}
                }
            \right]
            ,
            \quad
            E_{2} E_{1} A
            =
            \left[
                \phantom{
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    0 & 1 & 0 \\
                    1 & 0 & 1 \\
                \end{array}
                }
            \right]
            ,
        \end{gathered}
    \end{equation*}}

    \only<2->{Then 
    \begin{equation*}
        A 
        = 
        (E_p \dots E_1)^{-1} U
        =
        L U,
    \end{equation*}}

    \only<3->{Note that
    \begin{equation*}
        E_p \dots E_1 L
        = 
        E_p \dots E_1 (E_p \dots E_1)^{-1}
        =
        E_p \dots E_1 E_1^{-1} \dots E_p^{-1}
        = 
        I
        .
    \end{equation*}
    \think{} Why is $(E_p \dots E_1)^{-1}$ also \emph{unit lower triangular}?}
\end{frame}

\begin{frame}[c]
    \frametitle{The algorithm}
    
    To find a LU factorization of $A$
    \begin{enumerate}
        \item first reduce $A$ to an echelon form $U$ using \emph{only} row
            replacement,
        \item find $L$ which is reduced to $I$ using the same operations.
    \end{enumerate}

    \bomb{} Step 1 is not always possible.
\end{frame}

\begin{frame}[c]
    \frametitle{Example 2}
    
    \only<1>{
        \begin{equation*}
            A 
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                -4 & -5 & 3 & -8 & 1 \\
                2 & -5 & -4 & 1 & 8 \\
                -6 & 0 & 7 & -3 & 1 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L 
            =
            \begin{bmatrix}
                1 &   &   &   &   \\
                -2& 1 &   &   &   \\
                1 &   & 1 &   &   \\
                -3&   &   & 1 &   \\
            \end{bmatrix}
        \end{equation*}
    }

    \only<2>{
        \begin{equation*}
            A_{1}
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                0 & 3 & 1 & 2 & -3 \\
                0 & -9 & -3 & -4 & 10 \\
                0 & 12 & 4 & 12 & -5 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L 
            =
            \begin{bmatrix}
                1 &   &   &   &   \\
                -2& 1 &   &   &   \\
                1 &-3 & 1 &   &   \\
                -3& 4 &   & 1 &   \\
            \end{bmatrix}
        \end{equation*}
    }

    \only<3>{
        \begin{equation*}
            A_{2}
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                0 & 3 & 1 & 2 & -3 \\
                0 & 0 & 0 & 2 & 1 \\
                0 & 0 & 0 & 4 & 7 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L 
            =
            \begin{bmatrix}
                1 &   &   &   &   \\
                -2& 1 &   &   &   \\
                1 &-3 & 1 &   &   \\
                -3& 4 & 2 & 1 &   \\
            \end{bmatrix}
        \end{equation*}
    }

    \only<4>{
        \begin{equation*}
            U
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                0 & 3 & 1 & 2 & -3 \\
                0 & 0 & 0 & 2 & 1 \\
                0 & 0 & 0 & 0 & 5 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L 
            =
            \begin{bmatrix}
                1 &   &   &   &   \\
                -2& 1 &   &   &   \\
                1 &-3 & 1 &   &   \\
                -3& 4 & 2 & 1 &   \\
            \end{bmatrix}
        \end{equation*}
    }
\end{frame}

\begin{frame}[c]
    \frametitle{Example 2 recap}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{LU-02.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}
     
    Let $A$ be a lower triangular $n \times n$ matrix with nonzero entries
    on the diagonal.
    Show that $A$ is invertible and $A^{-1}$ is also lower triangular. 

    \begin{enumerate}
        \item Why $A$ can be reduced to $I$ using only row replacements and scaling?
        \item Why applying these operations to $I$ gives a lower triangular array?
        \item Do you remember the algorithm to find $A^{-1}$?
    \end{enumerate}
\end{frame}

\section{2.6 The Leontief Input-Output Model}%

\begin{frame}
    \frametitle{How much do we need?}

    Your country has three industries, \emoji{iphone}, \emoji{hamburger} and
    \emoji{film-frames}.
    For the next year, your people demand 50 millions \emoji{iphone},
    30 millions \emoji{hamburger}, and 20 millions \emoji{film-frames}.
    Producing goods also consumes goods.
    \begin{table}[]
        \scriptsize{}
        \begin{tabular}{@{}cccc@{}}
            \toprule
            \multirow{2}{*}{Input} & \multicolumn{3}{c}{Output}                                                 \\ \cmidrule(l){2-4} 
                               & \multicolumn{1}{l}{\emoji{iphone}}  & \multicolumn{1}{l}{\emoji{hamburger}} & \multicolumn{1}{l}{\emoji{film-frames}} \\ \midrule
            \emoji{iphone}     & 0.5   & 0.4   & 0.2   \\
            \emoji{hamburger}  & 0.2   & 0.3   & 0.1   \\
            \emoji{film-frames}& 0.1   & 0.1   & 0.3   \\ \bottomrule
        \end{tabular}
    \end{table}
    How many \emoji{iphone}, \emoji{hamburger}, \emoji{film-frames} dose your country
    needs to produce next year?
\end{frame}

\begin{frame}[c]
    \frametitle{The question}
    
    Let 
    $
    \bfx
        =
        \begin{bmatrix}
            x_\text{\emoji{iphone}} \\ x_\text{\emoji{hamburger}} \\ x_\text{\emoji{film-frames}}
        \end{bmatrix}
    $ 
    be the \alert{production vector} that lists the output.

    \pause{}

    Let
    $
    \bfd
        =
        \begin{bmatrix}
            d_\text{\emoji{iphone}} \\ d_\text{\emoji{hamburger}} \\ d_\text{\emoji{film-frames}}
        \end{bmatrix}
    $ 
    be the \alert{final demand vector} that lists the demand.

    \pause{}

    Let \alert{intermediate demand} be the need of each sector to produce $\bfd$.

    \pause{}

    Leontief asked can we find $\bfx$ such that
    \begin{equation*}
        \bfx = \text{intermediate demand} + \bfd.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Consumption vectors}

    Leontief's model assumes that each sector has a \alert{consumption vector}
    which lists the input needed for one output.

    \begin{table}[]
        \begin{tabular}{@{}cccc@{}}
            \toprule
            \multirow{2}{*}{Input} & \multicolumn{3}{c}{Output}                                                 \\ \cmidrule(l){2-4} 
                                   & \multicolumn{1}{l}{\emoji{iphone}}  & \multicolumn{1}{l}{\emoji{hamburger}} & \multicolumn{1}{l}{\emoji{film-frames}} \\ \midrule
            \emoji{iphone}     & 0.5   & 0.4   & 0.2   \\
            \emoji{hamburger}  & 0.2   & 0.3   & 0.1   \\
            \emoji{film-frames}& 0.1   & 0.1   & 0.3   \\ \bottomrule
                               & $\bfc_\text{\emoji{iphone}}$  & $\bfc_\text{\emoji{hamburger}}$  & $\bfc_\text{\emoji{film-frames}}$  \\
        \end{tabular}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{The equation}
    Then
    \begin{equation*}
        \begin{aligned}
        \text{intermediate demand} 
        &
        = x_\text{\emoji{iphone}} \bfc_\text{\emoji{iphone}} + x_\text{\emoji{hamburger}} \bfc_\text{\emoji{hamburger}} + x_\text{\emoji{film-frames}} \bfc_\text{\emoji{film-frames}}
        \\
        &
        = 
        \begin{bmatrix}
            \bfc_\text{\emoji{iphone}} & \bfc_\text{\emoji{hamburger}} & \bfc_\text{\emoji{film-frames}}
        \end{bmatrix}
        \begin{bmatrix}
            x_\text{\emoji{iphone}} \\ x_\text{\emoji{hamburger}} \\ x_\text{\emoji{film-frames}}
        \end{bmatrix}
        = C \bfx,
        \end{aligned}
    \end{equation*}
    where $C$ is the \alert{consumption} matrix.

    \pause{}

    So, the Leontief input-output model is
    \begin{equation*}
        \bfx = C \bfx + \bfd
        \Leftrightarrow
        I \bfx = C \bfx + \bfd
        \Leftrightarrow
        (I-C) \bfx = \bfd
    \end{equation*}
    So the solution is
    \begin{equation*}
        \bfx = (I- C)^{-1} \bfd.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Back to the example}
    
    In the example
    \begin{equation*}
        C =
        \begin{bmatrix}
            0.5   & 0.4   & 0.2   \\
            0.2   & 0.3   & 0.1   \\
            0.1   & 0.1   & 0.3   \\
        \end{bmatrix}
        ,
        \qquad
        \bfd
        =
        \begin{bmatrix}
            50 \\ 30 \\ 20
        \end{bmatrix}
        .
    \end{equation*}
    So the solution is
    \begin{equation*}
        \bfx 
        =
        (I-C)^{-1} \bfd 
        =
        \left[
            \begin{array}{c}
                \frac{6100}{27} \\
                \frac{3200}{27} \\
                \frac{700}{9} \\
            \end{array}
        \right]
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The existence of the solution}
    
    \begin{block}{Theorem 11}
        If the sum of each column in $C$ is less than $1$,
        then $(I-C)^{-1}$ exists.
    \end{block}

    \pause{}

    The idea is
    \begin{equation*}
        \begin{aligned}
            \bfx & = \bfd + C\bfd + C^{2} \bfd + \dots \\
                 & = (I + C + C^{2} + \dots ) \bfd \\
                 & = (I - C)^{-1} \bfd
        \end{aligned}
    \end{equation*}

    When $C$ satisfies the condition in Theorem 11, we have
    \begin{equation*}
        I + C + C^{2} + \dots 
        =
        (I - C)^{-1}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Approximate solution}

    So we can take a large $m$ and approximate by
    \begin{equation*}
        (I - C)^{-1}
        \approx
        I + C + C^{2} + \dots C^{m}
        .
    \end{equation*}

    In the example, we get
    \begin{equation*}
        \bfx
        \approx
        (I + C + C^{2} + \dots C^{20})\bfd
        =
        \left[
            \begin{array}{c}
                225.86973780559342 \\
                118.49042445835231 \\
                77.7596360015923 \\
            \end{array}
        \right]
        =
        \dot{\bfx}.
    \end{equation*}
    The error is
    \begin{equation*}
        \bfx - \dot{\bfx}
        =
        \left[
            \begin{array}{c}
                0.056188120332507197 \\
                0.028094060166210966 \\
                0.01814177618547319 \\
            \end{array}
        \right]
    \end{equation*}
\end{frame}

\end{document}
