# # Lecture 03

using LinearAlgebra, RowEchelon, SymPy

## Matrix multiplication
A = [1 2 -1;
     0 -5 3]
x = [4; 
    3; 
    7]
A * x

## Matrix multiplication
A = [2 -3;
     8 0;
     -5 2]
x = [4; 
    7]
A * x

## When is the system consistent?
@syms b[1:3]
A = [1 3 4 b[1];
    -4 2 -6 b[2];
    -3 -2 -7 b[3]]
A.echelon_form()

## Homogeneous system
A = [3 5 -4;
    -3 -2 4;
    6 1 -8];
rref(A)

## Nonhonogeneous system
A = [3 5 -4 7;
    -3 -2 4 -1
    6 1 -8 -4]
rref(A)

## Exercise
A = [1 3 1 1;
    -4 -9 2 -1;
    0 -3 -6 -3]
rref(A)

# # Propane gas

## Reduce to echelon form
@syms x[1:4]
A = [3 0 -1 0;
    8 0 0 -2;
    0 2 -2 -1]
A1 = rref(A)

## The equations
xs = sympy.Matrix([x[i] for i in 1:4])
@syms xs[1:4]
A2 = A1 * xs

## Solve the equations
sympy.solve(A2, xs[1:3])