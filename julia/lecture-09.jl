# # Lecture 09
using LinearAlgebra, RowEchelon

# ## LU Partition

# ### An example

## Factorize
A = [3 -7 -2 2; -3 5 1 0;6 -4 0 -5; -9 5 -5 12];
F = lu(A, Val(false))

## Check
A == F.L * F.U

## Solve equations -- method 1
b = [-9; 5; 7; 11];
x1 = round.(A \ b)

## Solve questions -- LU way
y = F.L \ b;
x2 = F.U \ y

# ### The idea

## Reduce A to echelon form
A = reshape(Vector(1:9), (3,3))
E1 = [1 0 0; -2 1 0; -3 0 1]
A1 = E1 * A
E2 = [1 0 0; 0 1 0; 0 -2 1]
A2 = E2 * A1

# ### An example of the algorithm

## Factorize
A = [2 4 -1 5 -2;
     -4 -5 3 -8 1;
     2 -5 -4 1 8;
     -6 0 7 -3 1]

## Step 1
E1 = [1 0 0 0;
      2 1 0 0;
      -1 0 1 0;
      3 0 0 1];
A1 = E1 * A

## Step 2
E2 = [1 0 0 0;
      0 1 0 0;
      0 3 1 0;
      0 -4 0 1];
A2 = E2 * A1

## Step 3
E3 = [1 0 0 0;
      0 1 0 0;
      0 0 1 0;
      0 0 -2 1];
A3 = E3 * A2

# ## Input-out put model

## The example
C = rationalize.([.5 .4 .2; .2 .3 .1; .1 .1 .3])
d = [50; 30; 20]
C1 = inv(I - C)
x = C1 * d

## Approximate solution
C = convert.(Float64, C)
C2 = sum([C^i for i in 0:30])
x1 = C2 * d
x1 - x