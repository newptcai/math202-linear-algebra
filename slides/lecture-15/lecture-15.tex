\input{../meta.tex}

\title{Lecture 15 --- Coordinate Systems}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}

    \tableofcontents{}
\end{frame}

\section{4.4 Coordinate Systems}

\subsection{Coordinates}

\begin{frame}
    \frametitle{The Unique Representation Theorem}
    
    \begin{block}{Theorem 7}
        Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $V$.
        Then for each $\bfx \in V$, there exists a \emph{unique} set of scalars
        $c_1, \dots, c_n$
        such that
        \begin{equation*}
            \bfx = c_1 \bfb_1 + \dots c_n \bfb_n.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Coordinates}
    
    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $V$.
    The \alert{coordinates of $\bfx$ relative to $\scB$}
    are the scalars $c_1, \dots, c_n$ such that
    \begin{equation*}
        \bfx = c_1 \bfb_1 + \dots c_n \bfb_n.
    \end{equation*}

    \pause{}

    We denote this by
    \begin{equation*}
        [\bfx]_{\scB}
        =
        \begin{bmatrix}
            c_1 \\ \vdots \\ c_{n}
        \end{bmatrix}
        .
    \end{equation*}

    The mapping $\bfx \mapsto [\bfx]_{\scB}$ is the \alert{coordinate mapping}.
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    Consider a basis for $\dsR^{2}$
    \begin{equation*}
        \scB = 
        \left\{
            \begin{bmatrix}
                1 \\ 0
            \end{bmatrix}
            ,
            \begin{bmatrix}
                1 \\ 2
            \end{bmatrix}
        \right\}
        .
    \end{equation*}
    What is $\bfx$ if
    \begin{equation*}
        [\bfx]_{\scB}
        =
        \begin{bmatrix}
            -2 \\ 3
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    
    \cake{} What is the coordinate of $\bfx = \begin{bmatrix} 1 \\ 6 \end{bmatrix}$
    relative to the standard basis $\scE = \{\bfe_1, \bfe_2\}$?
\end{frame}

\begin{frame}
    \frametitle{A graphical interpretation}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{coordinate-graphic.png}
    \end{figure}
\end{frame}

\subsection{Coordinates in $\dsR^{n}$}

\begin{frame}
    \frametitle{Coordinate mappings in $\dsR^{n}$}
    
    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $\dsR^{n}$.
    Let
    \begin{equation*}
        P_{\scB} = 
        \begin{bmatrix}
            \bfb_{1} & \dots & \bfb_{n}
        \end{bmatrix}
        .
    \end{equation*}
    Then
    \begin{equation*}
        \bfx = P_{\scB} \cdot [\bfx]_{\scB},
    \end{equation*}
    and
    \begin{equation*}
        [\bfx]_{\scB} = (P_{\scB})^{-1} \cdot \bfx.
    \end{equation*}
    We call $P_{\scB}$ the \alert{change of coordinates matrix} from $\scB$ to
    $\scE$.

    Note that $\bfx \mapsto [\bfx]_{\scB}$ is a one-to-one and onto linear transformation.
\end{frame}

\begin{frame}
    \frametitle{Example 4}
    
    Let
    \begin{equation*}
        \scB =
        \left\{
            \begin{bmatrix}
                2 \\ 1
            \end{bmatrix}
            ,
            \begin{bmatrix}
                -1 \\ 1
            \end{bmatrix}
        \right\}
        ,
        \qquad
        \bfx =
        \begin{bmatrix}
            4 \\ 5
        \end{bmatrix}
        .
    \end{equation*}

    Then 
    \begin{equation*}
        [\bfx]_{\scB}
        =
        (P_{\scB})^{-1}
        \cdot
        \bfx
        =
        \begin{bmatrix}
            3 \\ 2
        \end{bmatrix}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{coordinate-change.png}
    \end{figure}
\end{frame}

\subsection*{The Coordinate Mapping}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}

    Why is the coordinate mapping one-to-one and onto?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{coordinate-mapping.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Linear transformations}

    \begin{block}{Theorem 8}
        Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $V$.
        The mapping $\bfx \mapsto [\bfx]_{\scB}$ is a one-to-one \emph{linear
            mapping} from
        $V$ onto $\dsR^{n}$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 5}
    
    A one-to-one linear mapping from a vector space $V$ onto a vector space $W$
    is called an \alert{isomorphism}.

    \pause{}

    Let $\scB$ be the standard basis of $\dsP_{3}$.
    Then for
    \begin{equation*}
        \bfp(t) = a_0 + a_1 t + a_2 t^2 + a_3 t^3
    \end{equation*}
    has the coordinates
    \begin{equation*}
        [\bfp]_{\scB}
        =
        \begin{bmatrix}
            a_0 \\ a_1 \\ a_2 \\ a_3.
        \end{bmatrix}
    \end{equation*}
    Thus is the mapping $\bfp \mapsto [\bfp]_{\scB}$ is an isomorphism from
    $\dsP_{3}$ to $\dsR^{3}$.
\end{frame}

\begin{frame}
    \frametitle{Example 6}
    
    Use coordinates vectors to show that
    \begin{equation*}
        1 + 2 t^2,
        \qquad
        4 + t + 5t^2,
        \qquad
        3 + 2t
    \end{equation*}
    are linearly independent.

    Ex.~26 A set of vectors are linearly independent 
    if and only if their coordinate vectors are linearly independent.
\end{frame}

\begin{frame}
    \frametitle{Example 7}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            Let
            \begin{equation*}
                \bfv_{1} =
                \begin{bmatrix}
                    3 \\ 6 \\ 2
                \end{bmatrix}
                ,
                \qquad
                \bfv_{2} =
                \begin{bmatrix}
                    -1 \\ 0 \\ 1
                \end{bmatrix}
                ,
                \qquad
                \bfx =
                \begin{bmatrix}
                    3 \\ 12 \\ 7
                \end{bmatrix}
                .
            \end{equation*}

            Then $\bfx \in \Span{\bfv_1, \bfv_2}$
            and
            $[\bfx]_{\{\bfv_{1}, \bfv_{2}\}} = \begin{bmatrix} 2 \\3 \end{bmatrix}$.
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{coordinate-r3.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%    
%    \think{}
%    Use coordinate vectors to show that the following is a basis of $\dsP_{2}$.
%    \begin{equation*}
%        \scB = 
%        \{1+t^2, t-3t^2, 1+t-3t^2\}.
%    \end{equation*}
%
%    \think{}
%    Find $\bfq \in \dsP_{2}$ such that
%    \begin{equation*}
%        [\bfq]_{\scB} = 
%        \begin{bmatrix}
%            -1 \\ 1 \\2
%        \end{bmatrix}
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Exercise}
%    
%    \sweat{}
%    Let $S$ be a finite set in a vector space $V$ with the property
%    that every $x \in V$ has a unique representation as a linear
%    combination of elements of $S$. Show that $S$ is a basis of $V$.
%\end{frame}

\end{document}
