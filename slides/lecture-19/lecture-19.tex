\input{../meta.tex}

\title{Lecture 19 --- Difference Equations, Markov Chains}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{4.8 Application to Difference Equations}

\subsection{Nonhomogeneous equations}

\begin{frame}
    \frametitle{Nonhomogeneous equations}

    The general solution of
    \begin{equation}
        \label{eq:nonhomogeneous}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        z_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        ,
    \end{equation}
    can be written as a particular solution of \eqref{eq:nonhomogeneous}
    plus an arbitrary solution of
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        .
    \end{equation*}

    This is analogous to nonhomogeneous linear systems.
\end{frame}

\begin{frame}
    \frametitle{Example 6}
    
    Find all solutions of
    \begin{equation}
        \label{eq:nonhomogeneous:1}
        y_{k+2}
        -
        4 y_{k+1}
        +
        3 y_{k}
        =
        -4 k
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        ,
    \end{equation}

    \hint{} $k^2$ is a solution to \eqref{eq:nonhomogeneous:1}.
\end{frame}

%\subsection{Reduction to Systems of First-Order Equations}
%
%\begin{frame}
%    \frametitle{Systems of first-order equations}
%    
%    Write the following equation 
%    \begin{equation}
%        y_{k+3}
%        -
%        2 y_{k+2}
%        -
%        5 y_{k+1}
%        +
%        6 y_{k}
%        =
%        0
%        ,
%        \qquad
%        \text{for all }
%        k \in \dsZ
%        ,
%    \end{equation}
%    as
%    \begin{equation*}
%        \bfx_{k+1} = A \bfx_{k}
%        ,
%        \qquad
%        \text{for all }
%        k \in \dsZ
%    \end{equation*}
%    where $\bfx_{k} = \begin{bmatrix} y_k \\ y_{k+1} \\ y_{k+2} \end{bmatrix}$.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Generalization}
%    
%    In general
%    \begin{equation}
%        y_{k+n}
%        +
%        a_{1} y_{k+n-1}
%        +
%        \dots
%        +
%        a_{n} y_{k}
%        =
%        0
%        ,
%        \qquad
%        \text{for all }
%        k \in \dsZ
%        ,
%    \end{equation}
%    can be written as
%    \begin{equation*}
%        \bfx_{k+1} = A \bfx_{k}
%    \end{equation*}
%    where
%    \begin{equation*}
%        \bfx_{k} = 
%        \begin{bmatrix}
%            y_{k}     \\
%            y_{k+1}   \\
%            \vdots    \\
%            y_{k+n-1} \\
%        \end{bmatrix}
%        ,
%        \qquad
%        A 
%        =
%        \begin{bmatrix}
%            0 & 1 & 0 & \dots & 0 \\
%            0 & 0 & 1 & \dots & 0 \\
%            \vdots &   & \ddots  &  & \vdots \\
%            0 & 0 & 0 & \dots & 1 \\
%            -a_{n} & -a_{n-1} & -a_{n-2} & \dots & -a_{1} \\
%        \end{bmatrix}
%    \end{equation*}
%\end{frame}


\section{4.9 Applications to Markov Chains}

\begin{frame}[c]
    \frametitle{Moving around}
    
    Assume that the annual percentage of  migration between city and suburbs is as
    follows

    \vspace{1em}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{city-suburb.png}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{How the population changes}
    
    If the first year there are $600$ citizens and $400$ suburbans,
    the numbers of the second year is
    \begin{equation*}
        \begin{bmatrix}
            .95 & .03 \\
            .05 & .97 \\
        \end{bmatrix}
        \begin{bmatrix}
            600 \\ 
            400
        \end{bmatrix}
        =
        \begin{bmatrix}
            582 \\
            418
        \end{bmatrix}
        .
    \end{equation*}

    \pause{}

    This can be written as in percentage as
    \begin{equation*}
        \begin{bmatrix}
            .95 & .03 \\
            .05 & .97 \\
        \end{bmatrix}
        \begin{bmatrix}
            .6 \\ 
            .4
        \end{bmatrix}
        =
        \begin{bmatrix}
            .582 \\
            .418
        \end{bmatrix}
        .
    \end{equation*}

    \pause{}

    And for the third year

    \begin{equation*}
        \begin{bmatrix}
            .95 & .03 \\
            .05 & .97 \\
        \end{bmatrix}^{2}
        \begin{bmatrix}
            .6 \\ 
            .4
        \end{bmatrix}
        =
        \begin{bmatrix}
            .56544 \\
            .43456 \\
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Markov Chain}
    
    A vector with nonnegative entries that add up to $1$ is a \alert{probability vector}.

    A square matrix whose columns are probability vectors is a \alert{stochastic matrix}.

    A \emph{Markov chain} is sequence of probability vectors $\bfx_{0}, \bfx_{1}, \dots$
    together with a stochastic matrix $P$ such that
    \begin{equation*}
        \bfx_{k + 1} = P \bfx_{k}, \qquad \text{for all } k \ge 0.
    \end{equation*}

    \pause{}
    In the population example
    \begin{equation*}
        P =
        \begin{bmatrix}
            .95 & .03 \\
            .05 & .97 \\
        \end{bmatrix}
        ,
        \quad
        \bfx_{0} =
        \begin{bmatrix}
            .6 \\ 
            .4
        \end{bmatrix}
        ,
        \quad
        \bfx_{1} =
        \begin{bmatrix}
            .582 \\
            .418
        \end{bmatrix}
        ,
        \quad
        \bfx_{2} =
        \begin{bmatrix}
            .56544 \\
            .43456 \\
        \end{bmatrix}
        \dots
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The distant future}

    In $100$ years, the population stabilizes
    \begin{equation*}
        \begin{bmatrix}
            \bfx_{97} &
            \bfx_{98} &
            \bfx_{99} &
            \bfx_{100}
        \end{bmatrix}
        =
        \begin{bmatrix}
            .37508 & .37507 & .37506 & .37506 \\
            .62492 & .62493 & .62494 & .62494 \\
        \end{bmatrix}
    \end{equation*}
    \pause{}
    In other words, $\bfx_{k} \to \bfq$ as $k \to \infty$,
    where $\bfq$ satisfies
    \begin{equation*}
        P
        \bfq
        =
        \begin{bmatrix}
            .95 & .03 \\
            .05 & .97 \\
        \end{bmatrix}
        \begin{bmatrix}
            q_1 \\ q_2
        \end{bmatrix}
        =
        \begin{bmatrix}
            q_1 \\ q_2
        \end{bmatrix}
        =
        \bfq
    \end{equation*}
    and that $q_{1} + q_{2} = 1$.
    Such $\bfq$ is called a \alert{steady-state} vector for $P$.

    \pause{}

    Solving this gives
    \begin{equation*}
        \bfq
        =
        \left[
            \begin{array}{c}
                \frac{3}{8} \\
                \frac{5}{8} \\
            \end{array}
        \right]
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Regular stochastic matrix}
    
    A stochastic matrix is \emph{regular} if all entries of $P^{k}$ contains strictly
    positive entries for some $k$. For example
    \begin{equation*}
        P = 
        \left[
            \begin{array}{ccc}
                0.5 & 0.5 & 0.3 \\
                0.3 & 0.8 & 0.3 \\
                0.2 & 0 & 0.4 \\
            \end{array}
        \right]
    \end{equation*}
    is regular because
    \begin{equation*}
        P^2 = 
        \left[
            \begin{array}{ccc}
                0.46 & 0.65 & 0.42 \\
                0.45 & 0.79 & 0.45 \\
                0.18 & 0.1 & 0.22 \\
            \end{array}
        \right]
    \end{equation*}
    \cake{} Can you think of a \emph{stochastic} matrix which is not regular?
\end{frame}

\begin{frame}
    \frametitle{Convergence to steady-state vector}
    
    \begin{block}{Theorem 18}
        If $P$ is a regular stochastic matrix,
        then $P$ has a \emph{unique} steady-state vector $\bfq$.

        Moreover, the Markov chain $\{\bfx_{k}\}$ of $P$ converges to $\bfq$
        regardless of the choice of $\bfx_0$.
    \end{block}

    \pause{}

    In the population example
    \begin{equation*}
        P =
        \begin{bmatrix}
            .95 & .03 \\
            .05 & .97 \\
        \end{bmatrix}
        ,
        \quad
        \bfq
        =
        \left[
            \begin{array}{c}
                \frac{3}{8} \\
                \frac{5}{8} \\
            \end{array}
        \right]
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example}
    
    The convergence to $\bfq$ does not depend on $\bfx_0$, e.g.,
    \begin{equation*}
        \begin{aligned}
            \bfx_{0} & =
            \begin{bmatrix}
                .6 \\ 
                .4
            \end{bmatrix}
            ,
            \dots
            ,
            \bfx_{100}
            =
            \begin{bmatrix}
                .37506 \\
                .62494 \\
            \end{bmatrix}
            \\
            \bfx_{0} & =
            \begin{bmatrix}
                .3 \\ 
                .7
            \end{bmatrix}
            ,
            \dots
            ,
            \bfx_{100}
            =
            \begin{bmatrix}
                0.37498 \\
                0.62502 \\
            \end{bmatrix}
            \\
            \bfx_{0} & =
            \begin{bmatrix}
                .2 \\ 
                .3
            \end{bmatrix}
            ,
            \dots
            ,
            \bfx_{100}
            =
            \begin{bmatrix}
                0.1875 \\
                0.3125 \\
            \end{bmatrix}
        \end{aligned}
    \end{equation*}

    \bomb{} Why does the last one look wrong?
\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}
    
    Show that every $2 \times 2$ stochastic matrix has at least one steady-state
    vector.

    \pause{}

    \hint{} Such a matrix can be written as
    \begin{equation*}
        A=
        \begin{bmatrix}
            1 - \alpha & \beta \\
            \alpha & 1-\beta
        \end{bmatrix}
        ,
    \end{equation*}
    where $0 \le \alpha, \beta \le 1$.

    \pause{}

    When $\alpha = \beta = 0$, we have
    \begin{equation*}
        A 
        \begin{bmatrix}
            q \\ 1-q
        \end{bmatrix}
        =
        \begin{bmatrix}
            q \\ 1-q
        \end{bmatrix}
    \end{equation*}
    by taking $q = \text{\question}$.

    \pause{}

    What $q$ should we take when $\alpha \ne 0$ or $\beta \ne 0$?
\end{frame}

\end{document}
