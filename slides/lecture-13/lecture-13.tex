\input{../meta.tex}

\title{Lecture 13 --- Null Spaces, Column Spaces}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{4.2 Null Spaces, Column Spaces, and Linear Transformations}

\subsection*{The Null Space of a Matrix}

\begin{frame}
    \frametitle{Null spaces}
    
    Let $A$ be a $m \times n$ matrix. The \alert{null space} of $A$ is the solution
    set of $A \bfx = \bfzero$, denoted by $\nulspace A$.

    \pause{}

    In other words
    \begin{equation*}
        \nulspace A = \{\bfx: \bfx \in \dsR^{n}, A \bfx = \bfzero\}
    \end{equation*}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{null-space.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    Is $\bfu$ in $\nulspace A$?
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccc}
                1 & -3 & -2 \\
                -5 & 9 & 1 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfu = 
        \left[
            \begin{array}{c}
                5 \\
                3 \\
                -2 \\
            \end{array}
        \right]
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Why null `space'?}
    
    \begin{block}{Theorem 2}
        The null space of $A$ is a subspace of $\dsR^{n}$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Is the following set a vector space?
    \begin{equation*}
        H
        =
        \left\{
            \left[
                \begin{array}{c}
                    a \\
                    b \\
                    c \\
                    d \\
                \end{array}
            \right]
            :
            a - 2b + 5c = d,
            c -a = b
        \right\}
        .
    \end{equation*}
\end{frame}

\subsection*{An Explicit Description of $\nulspace A$}

\begin{frame}
    \frametitle{Example 3}
    
    Finding a spanning set for the null space of the matrix
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccccc}
                -3 & 6 & -1 & 1 & -7 \\
                1 & -2 & 2 & 3 & -1 \\
                2 & -4 & 5 & 8 & -4 \\
            \end{array}
        \right]
        \pause
        \sim
        \left[
            \begin{array}{cccccc}
                1 & -2 & 0 & -1 & 3 \\
                0 & 0 & 1 & 2 & -2 \\
                0 & 0 & 0 & 0 & 0 \\
            \end{array}
        \right]
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{Two comments}
%    
%     The vectors $\bfu, \bfv, \bfw$ are linearly independent, since
%     \begin{equation*}
%        x_{2} \bfu + x_{4} \bfv + x_{5} \bfw = 0
%     \end{equation*}
%     has only trivial solution. (Why?)
%
%     The number of vectors in the spanning set equals the number of free variables.
%\end{frame}

\subsection*{The Column Space of a Matrix}

\begin{frame}
    \frametitle{Column spaces}
    
    Let $A$ be a $m \times n$ matrix. The \alert{column space} of $A$ is the set spanned
    by the columns of $A$, denoted by $\colspace A$.

    \pause{}

    In other words
    \begin{equation*}
        \colspace A = \{\bfb: \bfb = A \bfx \text{ for some } \bfx \in \dsR^{n}\}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Why column `space'?}
    
    \begin{block}{Theorem 3}
        The column space of $A$ is a subspace of $\dsR^{m}$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 4}
    
    Find $A$ such that 
    \begin{equation*}
        \colspace A =
        \left\{
            \begin{bmatrix}
                6a - b \\ 
                a + b \\
                -7 a \\
            \end{bmatrix}
            :
            a, b \in \dsR
        \right\}
        .
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{Example 5, 6, 7}
    \begin{columns}
        \begin{column}{0.4\textwidth}
            Let
            \begin{equation*}
                A =
                \left[
                    \begin{array}{cccc}
                        2 & 4 & -2 & 1 \\
                        -2 & -5 & 7 & 3 \\
                        3 & 7 & -8 & 6 \\
                    \end{array}
                \right]
            \end{equation*}
            The solution of $A \bfx = \bfzero$ is
            \begin{equation*}
                \begin{aligned}
                    x{_1} & = -9 \cdot x{_3} \\
                    x{_2} & = 5 \cdot x{_3} \\
                    x{_4} & = 0 \\
                \end{aligned}
            \end{equation*}
        \end{column}
        \begin{column}{0.6\textwidth}
            \begin{enumerate}
                \item<1-4, 7> $\colspace A$ is  subspace of \question
                \item<2-4, 7> $\nulspace A$ is  subspace of \question
                \item<3-4, 7> Find a non-zero vector in $\colspace $A.
                \item<4, 7> Find a non-zero vector in $\nulspace $A.
                \item<5, 7> Is $\bfu = \begin{bmatrix}3 \\ -2 \\ -1 \\ 0 \end{bmatrix}
                    \in \nulspace A$?
                \item<6, 7> Is $ \bfv = \begin{bmatrix} 3 \\ -1 \\3 \end{bmatrix} \in \colspace A$?
            \end{enumerate}
        \end{column}
    \end{columns}
\end{frame}

\subsection*{Kernel and Range of a Linear Transformation}

\begin{frame}
    \frametitle{Linear transformation of vector spaces}

    Recall that a transformation $T:\dsR^{m} \mapsto \dsR^{n}$ is \alert{linear} if
    for all $\bfu, \bfv \in \dsR^{m}$
    \begin{enumerate}[(i)]
        \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
        \item  $T(c \bfu) = c T(\bfu)$.
    \end{enumerate}

    \pause{}

    Let $V$ and $W$ be vector spaces.
    A transformation $T:V \mapsto W$ is \alert{linear} if
    for all $\bfu, \bfv \in V$
    \begin{enumerate}[(i)]
        \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
        \item  $T(c \bfu) = c T(\bfu)$.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Null spaces and ranges}
    
    Let $T: V \mapsto W$ be a linear transformations. 

    The \alert{null space/kernel} of $T$ is the solution
    \begin{equation*}
        \{\bfv \in V: T(\bfv) = \bfzero\}.
    \end{equation*}

    \pause{}

    The \alert{range} of $T$ is the subset of vectors 
    \begin{equation*}
        \{\bfw \in W: T(\bfv) = \bfw \text{ for some } \bfv \in V\}.
    \end{equation*}

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{kernel-range.png}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%    
%    Let $V$ be the set of differential functions on $[a,b]$.
%
%    Let $W$ be the set of continuous functions on $[a,b]$.
%
%    For $f \in V$, let $D(f) = f'$.
%
%    \think{} Is $D$ a linear transformation?
%\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}
    
    Let $T: V \mapsto W$ be a \emph{linear transformation} from a vector space $V$ to
    another vector space $W$.

    \begin{enumerate}
        \item Show that the \emph{kernel} of $T$ is a subspace of $V$.
        \item Show that the \emph{range} of $T$ is a subspace of $W$.
    \end{enumerate}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%    
%    Consider
%    \begin{equation*}
%        y''(t) + \omega^{2} y(t) = 0
%    \end{equation*}
%    where $\omega$ is a constant.
%
%    \cake{} The solution set of this equation is the kernel of the linear
%    transformation \question{}.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Exercise}
%    
%    Let $V$ and $W$ be vector spaces, and let $T: V \mapsto W$ 
%    be a linear transformation. 
%
%    Given a subspace $U$ of $V$, 
%    let $T(U)$ denote the set of all images of the form $T(x)$, 
%    where $x \in U$. 
%
%    \think{} Show that $T(U)$ is a subspace of $W$.
%\end{frame}

\end{document}
