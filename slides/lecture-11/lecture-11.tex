\input{../meta.tex}

\title{Lecture 11 --- Cramer's Rule}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{3.3 Cramer's Rule, Volume and Linear Transformations}

\subsection{Cramer's Rule}

\begin{frame}
    \frametitle{\testtube{} An experiment}
    
    Let $A$ be an $n \times n$ matrix.

    Let $\bfe_{1}, \dots, \bfe_{n}$ be the role columns of $I_n$.

    What are $\bfe_{j} A$ and $A \bfe_{j}$?
\end{frame}

\begin{frame}
    \frametitle{A notation}
    
    Let
    \begin{equation*}
        A = \begin{bmatrix} \bfa_1 & \bfa_2 & \dots & \bfa_n \end{bmatrix}.
    \end{equation*} 
    and
    \begin{equation*}
        A_i(\bfb) = 
        \begin{bmatrix}
            \bfa_1 & \dots & \bfa_{i-1} & \bfb & \bfa_{i+1} & \dots & \bfa_{n}
        \end{bmatrix}
        .
    \end{equation*}

    \pause{}
    
    Let
    \begin{equation*}
        A=
        \left[
            \begin{array}{ccc}
                0 & 0 & -1 \\
                0 & 3 & -1 \\
                3 & 3 & 0 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfb =
        \left[
            \begin{array}{c}
                0 \\
                3 \\
                2 \\
            \end{array}
        \right]
        .
    \end{equation*}
    Then
    \begin{equation*}
        A_{3}(\bfb) = 
        \left[
            \begin{array}{ccc}
                0 & 0 & 0 \\
                0 & 3 & 3 \\
                3 & 3 & 2 \\
            \end{array}
        \right]
    \end{equation*}
    \cake{} What is $A_{1}(\bfb), A_{2}(\bfb)$?
\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-pair-share}
    
    \begin{block}{\testtube{} Experiment}
        Let $I$ be the $3 \times 3$ identity matrix.
        Let $\bfx = \begin{bmatrix}1 \\ 2 \\ 3 \end{bmatrix}$.

        What are $\det(I_{1}(\bfx)), \det(I_2(\bfx)), \det(I_{3}(\bfx))$?

        \bomb{} Here $I_{1}, I_{2}, I_{3}$ are not identity matrices of different sizes.
    \end{block}

    \pause{}

    \begin{block}{\medal{} The problem}
        Let $I$ be the $n \times n$ identity matrix.
        Let $\bfx = \begin{bmatrix}x_1 \\ \vdots \\ x_n \end{bmatrix}$.

        What is $\det(I_{i}(\bfx))$? Why?
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Cramer's rule}
    
    \begin{block}{Theorem 7}
        If $A$ is invertible, then for any $\bfb$, the solution of $A \bfx = \bfb$ is
        \begin{equation*}
            \bfx = 
            \begin{bmatrix}
                \frac{\det A_{1}(\bfb)}{\det A}
                \\
                \vdots
                \\
                \frac{\det A_{n}(\bfb)}{\det A}
            \end{bmatrix}
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    \think{} Let 
    \begin{equation*}
        A = 
        \begin{bmatrix}
            3 & -2 \\
            -5 & 4 \\
        \end{bmatrix}
        ,
        \qquad
        \bfb
        =
        \begin{bmatrix}
            6 \\ 8
        \end{bmatrix}
        .
    \end{equation*}
    Use Cramer's Rule to solve $A \bfx = \bfb$.
\end{frame}

%\begin{frame}
%    \frametitle{Example 2}
%    
%    Use Cramer's rule to describe the solution sets.
%    \begin{equation*}
%        \systeme[x_1x_2]{
%            3 s x_1 -2 x_2 = 4,
%           -6 x_1 +s x_2 = 1
%        }
%    \end{equation*}
%\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{block}{Theorem 7}
                If $A$ is invertible, then for any $\bfb$, the solution of $A \bfx = \bfb$ is
                \begin{equation*}
                    \bfx = 
                    \begin{bmatrix}
                        \frac{\det A_{1}(\bfb)}{\det A}
                        \\
                        \vdots
                        \\
                        \frac{\det A_{n}(\bfb)}{\det A}
                    \end{bmatrix}
                \end{equation*}
            \end{block}
        \end{column}
        \begin{column}{0.5\textwidth}
            Let $A$ be an invertible matrix.
            Show that
            \begin{equation*}
                (A^{-1})_{i j} = \frac{\det A_{i}(\bfe_{j})}{\det A},
            \end{equation*}

            \pause{}

            \hint{} Hints:
            \begin{enumerate}[<+->]
                \item What is $A^{-1} \bfe_{j}$?
                \item What is the solution of $A \bfx = \bfe_{j}$?
                \item How to use Cramer's rule to compute this solution $\bfx$?
            \end{enumerate}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{An inverse formula}
    
    \begin{block}{Theorem 8}
        If $A$ is invertible, then
        \begin{equation*}
            A^{-1} =
            \frac{1}{\det A}
            \begin{bmatrix}
                C_{11} & C_{21} & \dots & C_{n1} \\
                C_{12} & C_{22} & \dots & C_{2n} \\
                \vdots & \vdots &       & \vdots \\
                C_{1n} & C_{2n} & \dots & C_{nn} \\
            \end{bmatrix}
            =
            \frac{1}{\det A}
            C
        \end{equation*}
        where
        \begin{equation*}
            C_{ij} = (-1)^{i+j} \det A_{ij}.
        \end{equation*}
    \end{block}
    The matrix $C$ is called the \alert{adjugate/classical adjoint} of $A$ and is denoted by
    $\adj A$.
\end{frame}

%\begin{frame}
%    \frametitle{Try the formula}
%    
%    Recall that
%    \begin{equation*}
%        C_{ij} = (-1)^{i+j} \det A_{ji}.
%    \end{equation*}
%    Let
%    \begin{equation*}
%        A =
%        \left[
%            \begin{array}{ccc}
%                2 & 1 & 3 \\
%                1 & -1 & 1 \\
%                1 & 4 & -2 \\
%            \end{array}
%        \right]
%        .
%    \end{equation*}
%    Then $\det A = 14$.
%    What is $C_{23}$ and what is $(A_{-1})_{23}$?
%\end{frame}

\subsection{Determinants as Area or Volume}

\begin{frame}
    \frametitle{Determinants as Area}

    \begin{block}{Theorem 9}
        If $A$ is a $2 \times 2$ matrix, 
        the area of the parallelogram determined by the columns of $A$ is $\abs{\det A}$. 
    \end{block}

    \begin{columns}
        \begin{column}{0.2\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{determinant-2d-1.png}
            \end{figure}        
        \end{column}
        \begin{column}{0.8\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{determinant-2d-2.png}
            \end{figure}       
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Determinants as Volume}

    \begin{block}{Theorem 9}
        If $A$ is a $3 \times 3$ matrix, 
        the volume of the parallelepiped determined by the columns of $A$ is
        $\abs{\det A}$.
    \end{block}

    \begin{columns}
        \begin{column}{0.3\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{determinant-2d-3.png}
            \end{figure}        
        \end{column}
        \begin{column}{0.7\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{determinant-2d-4.png}
            \end{figure}       
        \end{column}
    \end{columns}
\end{frame}

\subsection{Linear Transformation}

\begin{frame}
    \frametitle{Linear Transformation in $\dsR^{2}$}
    
    \begin{block}{Theorem 10}
        Let $T: \dsR^{2} \mapsto \dsR^{2}$ be a linear transform defined by $T(\bfx)
        = A\bfx$.
        Then for any $S \in \dsR^{2}$
        \begin{equation*}
            \area (T(S)) = \abs{\det(A)} \area S.
        \end{equation*}
    \end{block}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{determinant-2d-transform-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The area of an eclipse}

    What is the area of the eclipse in the picture?
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{determinant-2d-transform-2.png}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}

        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Linear Transformation in $\dsR^{3}$}
    
    \begin{block}{Theorem 10}
        Let $T: \dsR^{3} \mapsto \dsR^{3}$ be a linear transform defined by $T(\bfx)
        = A \bfx$.
        Then for any $V \in \dsR^{3}$
        \begin{equation*}
            \vol (T(V)) = \det(A) \vol V.
        \end{equation*}
    \end{block}
\end{frame}


%\begin{frame}
%    \frametitle{\sweat{} Think-Pair-Share}
%
%    What is the linear transformation to turn $S$ into $S'$?
%
%    What is $\vol(S')$?
%    
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{determinant-3d-transform-1.png}
%    \end{figure}
%\end{frame}

\end{document}
