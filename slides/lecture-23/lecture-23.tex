\input{../meta.tex}

\title{Lecture 23 --- Inner Product, Orthogonal Sets}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{6.1 Inner Product, Length, and Orthogonality}

\subsection{Inner Product}

\begin{frame}
    \frametitle{The inner product}
    
    If $\bfu, \bfv \in \dsR^{n}$, then the \alert{inner/dot product} of them is
    $\bfu^{T} \bfv$. In other words
    \begin{equation*}
        \bfu \cdot \bfv 
        =
        \begin{bmatrix}
            u_1 & \dots & u_n
        \end{bmatrix}
        \begin{bmatrix}
            v_1 \\ \vdots \\ v_n
        \end{bmatrix}
        =
        u_1 v_1 + \cdots + u_n v_n
        .
    \end{equation*}
    
    Let
    \begin{equation*}
        \bfu
        =
        \begin{bmatrix}
            2 \\ -5 \\ -1
        \end{bmatrix}
        ,
        \quad
        \bfv
        =
        \begin{bmatrix}
            3 \\ 2 \\ -3
        \end{bmatrix}
        .
    \end{equation*}
    \cake{} What are $\bfu \cdot \bfv$ and $\bfv \cdot \bfu$?
\end{frame}

\begin{frame}
    \frametitle{Properties of inner product}
    
    \begin{block}{Theorem 1}
        Let $\bfu, \bfv, \bfw \in \dsR^{n}$ and $c \in \dsR$.
        Then
        \vspace{-.5em}
        \begin{enumerate}[a.]
            \item $\bfu \cdot \bfv = \bfv \cdot \bfu$.
            \item $(\bfu+\bfv) \cdot \bfw = \bfu \cdot \bfw + \bfv \cdot \bfw$.
            \item $(c \bfu) \cdot \bfv = c (\bfu \cdot \bfv) = \bfu \cdot (c \bfv)$.
            \item $\bfu \cdot \bfu \ge 0$, and $\bfu \cdot \bfu = 0$ if and only if
                $\bfu = \bfzero$.
        \end{enumerate}
    \end{block}
\end{frame}

\subsection{The Length of a Vector}

\begin{frame}
    \frametitle{Length}
    
    The \alert{length/norm} of a vector $\bfv$ is defined by
    \begin{equation*}
        \norm{\bfv}
        =
        \sqrt{\bfv \cdot \bfv}
        ,
    \end{equation*}
    which implies that
    \begin{equation*}
        \norm{c \bfv} = \abs{c} \norm{\bfv}.
    \end{equation*}

    \cake{} If $\bfv = \begin{bmatrix} a \\ b \end{bmatrix}$, what is $\norm{v}$?

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{length-in-r2.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Unit vector}
    
    A vector whose length is $1$ is called a \alert{unit vector}.

    For any nonzero vector $\bfv$, $\norm{\bfv}^{-1} \bfv$ is a unit vector.
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \cake{} Let $\bfv = (1, -2, 2, 0)$. Find a unit vector $\bfu$ in the direction of
    $\bfv$.
\end{frame}

\begin{frame}
    \frametitle{Example 3}
    
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.7\textwidth]{6.1-01.png}
            \end{figure}
        \end{column}
        \begin{column}{0.6\textwidth}
        Let $W$ be the subspace of $\dsR^2$ spanned by $\bfx = (2/3, 1)$. 
        Find a unit vector $\bfz$ that is a basis for $W$.
        \end{column}
    \end{columns}
\end{frame}

\subsection{Distance in $\dsR^{n}$}

\begin{frame}
    \frametitle{Distance in $\dsR^{n}$}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{distance-in-r.png}
    \end{figure}

    For two real numbers $a$ and $b$, the distance between them is 
    \begin{equation*}
        \abs{a-b} = \norm{[a] - [b]}.
    \end{equation*}

    \pause{}

    For $\bfu, \bfv \in \dsR^{n}$, the \alert{distance between $\bfu$ and $\bfv$} is
    defined by
    \begin{equation*}
        \dist(\bfu, \bfv) = \norm{\bfu - \bfv}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    What is the distance between $\bfu = (7, 1)$ and $\bfv = (3,2)$?

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{distance-in-rn.png}
    \end{figure}
\end{frame}

\subsection{Orthogonal sets}

\begin{frame}
    \frametitle{Orthogonal vectors in $\dsR^{2}$}
    
    In $\dsR^{2}$, the lines determined by $\bfu$ and $\bfv$ are perpendicular
    if and only if
    \begin{equation*}
        \dist(\bfu, \bfv) = \dist(\bfu, -\bfv)
        \only<2>{\iff
        \bfu \cdot \bfv
        =
        0}
        .
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.5\linewidth]{orthognal.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Orthogonal vectors in $\dsR^{n}$}
    
    In $\dsR^{n}$, two vectors $\bfu$ and $\bfv$ are \alert{orthogonal} if $\bfu \cdot \bfv =
    0$.

    Thus $\bfzero$ is orthogonal to all vectors.
\end{frame}

\begin{frame}
    \frametitle{The Pythagorean Theorem}
    
    \begin{block}{Theorem 2}
        Two vectors $\bfu$ and $\bfv$ are orthogonal if and only if
        \begin{equation*}
            \norm{\bfu+\bfv}^{2}
            =
            \norm{\bfu}^{2}
            +
            \norm{\bfv}^{2}
        \end{equation*}
    \end{block}

    \begin{figure}
    \begin{center}
        \includegraphics[width=0.5\linewidth]{pythagoras.png}
    \end{center}
    \end{figure}
\end{frame}

\subsection{Orthogonal complements}

\begin{frame}
    \frametitle{Orthogonal complements}

    If $\bfz$ is orthogonal to every vector in subspace $W$ of $\dsR^{n}$,
    then we say that $\bfz$ is \alert{orthogonal} to $W$.

    The sets of all vectors which are orthogonal to $W$ is called its
    \alert{orthogonal complement},
    denoted by $W^{\perp}$

    \pause{}
    
    \begin{figure}
    \begin{center}
        \includegraphics[width=0.4\linewidth]{orthognal-complement.png}
    \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\cake{} Think-Pair-Share}

    Show that if $\bfx$ is in both $W$ and $W^{\perp}$, then $\bfx = \bfzero$.
\end{frame}

\begin{frame}
    \frametitle{Properties of orthogonal complements}
    
    \begin{enumerate}
        \item A vector $\bfx$ is in $W^{\perp}$ if and only if $\bfx$ is orthogonal to every vector in a set that spans $W$.
        \item $W^{\perp}$ is a subspace of $\dsR^n$.
    \end{enumerate}

    \begin{figure}
    \begin{center}
        \includegraphics[width=0.4\linewidth]{orthognal-complement.png}
    \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{$\nulspace A$, $\rowspace A$, $\colspace A$}
    
    \begin{block}{Theorem 3}
        Let $A$ be an $m \times n$ matrix. 
        Then
        \begin{equation*}
            (\rowspace A)^{\perp} = \nulspace A
            ,
            \qquad
            (\colspace A)^{\perp} = \nulspace A^{T}
            .
        \end{equation*}
    \end{block}

    \pause{}

    \begin{figure}
    \begin{center}
        \includegraphics[width=0.7\linewidth]{row-col-null.png}
    \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Angles}
    
    If $\bfu$ and $\bfv$ are in either $\dsR^2$ or $\dsR^3$, then
    \begin{equation*}
        \bfu \cdot \bfv
        =
        \norm{\bfu} \norm{\bfv} \cos \theta
        ,
    \end{equation*}
    where $\theta$ is the angel between the two.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{6.1-09.png}
    \end{figure}
\end{frame}

\end{document}
