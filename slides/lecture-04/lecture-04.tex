\input{../meta.tex}

\title{Lecture 04 --- Linear Independence}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{1.6 Applications of Linear Systems}

\begin{frame}
    \frametitle{How to make a barbecue? \emoji{cut-of-meat}}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{Propane_Gas_Grill.jpg}
        \caption*{\href{https://commons.wikimedia.org/wiki/File:Propane\_Gas\_Grill.jpg}{Propane Gas Grill}}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Balancing Chemical Equations}

    When propane gas burns
    \begin{equation*}
        (x_1) \chem{C_3 H_8} + (x_2) \chem{O_2}
        \to
        (x_3) \chem{CO_2} + (x_4) \chem{H_2 O}
    \end{equation*}

    \pause{}

    Balancing that equation means solving
    \begin{equation*}
        x_1 
        \begin{bmatrix}
            3 \\ 8 \\ 0
        \end{bmatrix}
        +
        x_2
        \begin{bmatrix}
            0 \\ 0 \\ 2
        \end{bmatrix}
        =
        x_3
        \begin{bmatrix}
            1 \\ 0 \\2 
        \end{bmatrix}
        +
        x_4
        \begin{bmatrix}
            0 \\ 2 \\ 1
        \end{bmatrix}
    \end{equation*}

    This is just a \question{}.
\end{frame}

\section{1.7 Linear Independence}

\begin{frame}[t]
    \frametitle{Example 1}

    \think{} Does the following \question{} linear system 
    has solutions other than the trivial $\bfzero$?
    \begin{equation*}
        x_1
        \begin{bmatrix}
            1 \\ 2 \\ 3
        \end{bmatrix}
        +
        x_2
        \begin{bmatrix}
            4 \\ 5 \\ 6
        \end{bmatrix}
        +
        x_3
        \begin{bmatrix}
            2 \\ 1 \\ 0
        \end{bmatrix}
        =
        \begin{bmatrix}
            0 \\ 0 \\ 0
        \end{bmatrix}
    \end{equation*}

    \pause{}

    The answer is \question{} because
    \begin{equation*}
        \begin{bmatrix}
            1 & 4 & 2 & 0 \\
            2 & 5 & 1 & 0 \\
            3 & 6 & 0 & 0 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & 4 & 2 & 0 \\
            0 & -3 & -3 & 0 \\
            0 & 0 & 0 & 0 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{Linear independence}

    A set of vectors $\{\bfv_{1}, \dots, \bfv_p\}$ is \alert{linearly independent}
    if
    \begin{equation*}
        x_1 \bfv_1 + x_2 \bfv_2 + \dots + x_p \bfv_p = \bfzero
    \end{equation*}
    has only the trivial solution.

    Otherwise it is \alert{linearly dependent}.

    \cake{} Are the vectors in Example 1 linearly independent?

\end{frame}

\begin{frame}[c]
    \frametitle{Linear independence --- Matrix columns}

    The columns of $A = \begin{bmatrix}\bfa_1 & \dots & \bfa_n\end{bmatrix}$ are \alert{linear independent} if
    $\{\bfa_{1}, \dots, \bfa_n\}$ are \alert{linearly independent}.
    In other words, if $A \bfx = \bfzero$ has \emph{only} the trivial solution.

    Otherwise its columns are \alert{linearly dependent}.

    \pause{}

    \cake{} Are the columns of the coefficient matrix in Example 1 linearly independent?
    \begin{equation*}
        \begin{bmatrix}
            1 & 4 & 2 \\
            2 & 5 & 1 \\
            3 & 6 & 0 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    
    \cake{} Is this linearly independent?
    \begin{equation*}
        A =
        \begin{bmatrix}
            0 & 1 & 4 \\
            1 & 2 & -1 \\
            5 & 8 & 0 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\subsection{Sets of One or Two Vectors}

\begin{frame}
    \frametitle{Two vectors}

    The following vectors are \emph{linearly dependent}
    \begin{equation*}
        \bfv_1 = \begin{bmatrix}
            3 \\ 1
        \end{bmatrix}
        ,
        \bfv_2 = \begin{bmatrix}
            6 \\ 2
        \end{bmatrix}
    \end{equation*}

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{linearly-independent-01.png}
        \caption*{Linearly dependent}%
    \end{figure}

    \emoji{bulb} Two vectors is linearly dependent if and only if one is
    a multiple of the other.
\end{frame}

\begin{frame}
    \frametitle{Two vectors}

    The following vectors are \emph{linearly independent}
    \begin{equation*}
        \bfv_1 = \begin{bmatrix}
            3 \\ 2
        \end{bmatrix}
        ,
        \bfv_2 = \begin{bmatrix}
            6 \\ 2
        \end{bmatrix}
    \end{equation*}

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{linearly-independent-02.png}
        \caption*{Linearly independent}%
    \end{figure}

    \emoji{bulb} Two vectors is linearly independent if and only if neither is
    a multiple of the other.
\end{frame}

\subsection{Sets of Two or More Vectors}

\begin{frame}
    \frametitle{Two or more vectors}
    \begin{block}{Theorem 7}
        A set of two or more vectors $S = \{\bfv_{1}, \dots, \bfv_{p}\}$
        is \emph{linearly dependent} if and only if
        \emph{at least one vector} in $S$ is a \emph{linear combination}
        of the others.

        \pause{}

        If $S$ is linearly dependent and $\bfv_1 \ne \bfzero$,
        then some $\bfv_j$ with ($j > 1$) is a linear combination of
        the preceding vectors, $\bfv_1, \dots, \bfv_{j-1}$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Consider $\bfu$ and $\bfv$ in the pictures.
    Why is $\bfw \in \Span{\bfu, \bfv}$ if and only if the three is linearly
    dependent?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linearly-independent-03.png}
        \caption*{Three linearly dependent vectors}%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Simple case \emoji{smile}}

    \begin{block}{Theorem 8}
        Any set $\{\bfv_{1}, \dots, \bfv_{p}\} \subseteq \dsR^{n}$
        is \emph{linearly dependent} if $p > n$.
    \end{block}
\end{frame}

\begin{frame}[t]
    \frametitle{Even simpler case \emoji{laughing}}

    \begin{block}{Theorem 9}
        If $S = \{\bfv_{1},\dots, \bfv_{p}\}$ contains $\bfzero$,
        then the set is \emph{linearly dependent}.
    \end{block}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 6}

    \cake{} Are the columns of these matrices linearly dependent?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linearly-depedent-example-6.png}
    \end{figure}

    \pause{}

    \begin{block}{\hint{} Theorem 8}
        Any set $\{\bfv_{1}, \dots, \bfv_{p}\} \subseteq \dsR^{n}$
        is \emph{linearly dependent} if $p > n$.
    \end{block}

    \begin{block}{\hint{} Theorem 9}
        If $S = \{\bfv_{1},\dots, \bfv_{p}\}$ contains $\bfzero$,
        then the set is \emph{linearly dependent}.
    \end{block}
\end{frame}

\begin{frame}[t]
    \frametitle{Think-Pair-Share \sweat{}}

    1. Suppose $A$ is a $m \times n$ matrix with the property that for all $\bfb \in
    \dsR^{n}$, $A \bfx = \bfb$ has \emph{at most one} solution.
    Are the columns of $A$ linearly independent?

    \pause{}

    2. Suppose an $m \times n$ matrix $A$ has $n$ pivot columns.
    Explain why for each $\bfb \in \dsR^m$ the equation $A \bfx = \bfb$
    has at most one solution.

    \hint{} Explain why $A \bfx = \bfb$ cannot have infinitely many solutions
    for any $\bfb \in \dsR$?
\end{frame}

\end{document}
