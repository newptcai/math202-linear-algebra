\input{../meta.tex}

\title{Lecture 14 --- Linearly Independent Sets; Bases}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{4.3 Linearly Independent Sets; Bases}

\begin{frame}
    \frametitle{Linear independence in $\dsR^{n}$}

    A set of vectors $\{\bfv_{1}, \dots, \bfv_p\}$ in $\dsR^{n}$ 
    is \alert{linearly independent}
    if
    \begin{equation*}
        c_1 \bfv_1 + c_2 \bfv_2 + \dots + c_p \bfv_v = \bfzero
    \end{equation*}
    has only the trivial solution.
    Otherwise it is \alert{linearly dependent}.
\end{frame}

\begin{frame}
    \frametitle{Linear independence in vector spaces}
    
    A set of vectors $\{\bfv_{1}, \dots, \bfv_p\}$ in a vector space $V$ 
    is \alert{linearly independent}
    if
    \begin{equation*}
        c_1 \bfv_1 + c_2 \bfv_2 + \dots + c_p \bfv_v = \bfzero
    \end{equation*}
    has only the trivial solution.
    Otherwise it is \alert{linearly dependent}.

    \pause{}

    \dizzy{} Is $\{ \bfzero \}$ linearly independent?
\end{frame}

\begin{frame}
    \frametitle{When are vectors linearly independent?}

    \begin{block}{Theorem 7 (Ch.~1) and Theorem 4 (Ch.~4)}
        A set of two or more vectors $\{\bfv_{1}, \dots, \bfv_{p}\}$ 
        with $\bfv_{1} \ne \bfzero$
        is \emph{linearly dependent} if and only if
        some $\bfv_{j}$ with $j > 1$ is a linear combination of $\bfv_{1}, \dots,
        \bfv_{j-1}$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    Let $\bfp_{1}(t) = 1$, $\bfp_{2}(t) = t$ 
    and $\bfp_{3}(t) = 4 - t$.
    Are they linearly independent?
\end{frame}

%\begin{frame}
%    \frametitle{Example 2}
%
%    Consider $\{\sin t, \cos t\}$ in $C[0,1]$.
%    Are they linearly independent?
%    What about $\{\sin 2t, \sin t \cos t\}$?
%\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}
    
    Let $T:V \mapsto W$ be a linear transform.
    Further assume that $T$ is \emph{one-to-one}.
    Show that if 
    $\{T(\bfv_{1}), \ldots, T(\bfv_{p})\}$
    is linearly dependent,
    the so is
    $\{\bfv_{1}, \ldots, \bfv_{p}\}$.
\end{frame}

\subsection*{Bases}

\begin{frame}
    \frametitle{Bases}
    
    Let $H$ be a subspace of $V$.
    A set of vectors $\scB = \{\bfv_{1}, \dots, \bfv_{p}\}$ is a \alert{basis} of $H$
    if
    \begin{enumerate}[(i)]
        \item $\scB$ is \emph{linearly independent},
        \item and $\Span{\scB} = H$.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Example 3}
    
    Let $A = [\bfa_{1} \, \ldots \, \bfa_{n}]$ be an invertible $n \times n$ matrix.

    \think{} Are the columns of $A$ a basis for $\dsR^{n}$?
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            The set $\{\bfe_{1}, \dots, \bfe_{n}\}$ 
            is called the \alert{standard basis} of $\dsR^{n}$
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{basis-standard.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{Example 5}
%
%    \think{} Is the following a basis of $\dsR^{3}$?
%    \begin{equation*}
%        \bfv_{1} = 
%        \begin{bmatrix}
%            3 \\ 0 \\ -6
%        \end{bmatrix}
%        , 
%        \qquad
%        \bfv_{2} = 
%        \begin{bmatrix}
%            -4 \\ 1 \\ 7
%        \end{bmatrix}
%        , 
%        \qquad
%        \bfv_{3} = 
%        \begin{bmatrix}
%            -2 \\ 1 \\ 5
%        \end{bmatrix}
%        .
%    \end{equation*}
%\end{frame}

\begin{frame}
    \frametitle{Example 6}
    The set $S = \{1, t, t^{2}, \dots, t^{n}\}$ 
        is called the \alert{standard basis} of $\dsP^{n}$.
\end{frame}

\subsection*{The Spanning Set Theorem}

\begin{frame}
    \frametitle{Example 7}
    
    Let
    \begin{equation*}
        \bfv_{1} = 
        \begin{bmatrix}
            0 \\ 2 \\ -1
        \end{bmatrix}
        , 
        \qquad
        \bfv_{2} = 
        \begin{bmatrix}
            2 \\ 2 \\ 0
        \end{bmatrix}
        , 
        \qquad
        \bfv_{3} = 
        \begin{bmatrix}
            6 \\ 16 \\ -5
        \end{bmatrix}
        ,
    \end{equation*}
    and $H = \Span{\bfv_1, \bfv_2, \bfv_3}$.
    Show that $\{\bfv_{1}, \bfv_{2}\}$ is a basis of $H$.

    \hint{} $\bfv_3 = 5 \bfv_1 + 3 \bfv_2$.
\end{frame}

\begin{frame}
    \frametitle{The Spanning Set Theorem}
    
    \begin{block}{Theorem 5}
        Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$ 
        and $H = \Span{\bfv_{1}, \dots, \bfv_{p}}$.
        \begin{enumerate}[<+->]
            \item[a.] If a vector in $S$ is the linear combination of other vectors
                in $S$, then it can be removed so that the remaining vectors still span $H$.
            \item[b.] If $H \ne \{\bfzero\}$, then some subsets of $S$ is a basis of $H$.
        \end{enumerate}
    \end{block}
\end{frame}

\subsection*{Bases for $\nulspace A$ and $\colspace A$}

\begin{frame}
    \frametitle{Bases of null spaces (Example 3, Ch.~4.2)}
    
    Finding a spanning set for the null space of the matrix
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccccc}
                -3 & 6 & -1 & 1 & -7 \\
                1 & -2 & 2 & 3 & -1 \\
                2 & -4 & 5 & 8 & -4 \\
            \end{array}
        \right]
    \end{equation*}
    amounts to solve $A \bfx = \bfzero$, \pause{} which gives
    \begin{equation*}
        \bfx 
        =
        \begin{bmatrix}
            2 x_2 + x_4 - 3 x_5 \\
            x_2 \\
            -2 x_4 + 2 x_5 \\
            x_{4} \\
            x_{5} \\
        \end{bmatrix}
        =
        x_{2}
        \begin{bmatrix}
            2 \\ 1 \\ 0 \\ 0 \\ 0
        \end{bmatrix}
        +
        x_{4}
        \begin{bmatrix}
            1 \\ 0 \\ -2 \\ 1 \\ 0
        \end{bmatrix}
        +
        x_{5}
        \begin{bmatrix}
            -3 \\ 0 \\ 2 \\ 0 \\ 1
        \end{bmatrix}
        = 
        x_{2} \bfu + x_{4} \bfv + x_{5} \bfw,
    \end{equation*}
    and $\nulspace A = \Span{\bfu, \bfv, \bfw}$.

    \cake{} Since $\{\bfu, \bfv, \bfw\}$ is linearly independent,
    it is a basis of $\nulspace A$.
\end{frame}

\begin{frame}
    \frametitle{Example 8}

    Let
    \begin{equation*}
        B = 
        \begin{bmatrix}
            1 & 4 & 0 &  2 & 0\\
            0 & 0 & 1 & -1 & 0 \\
            0 & 0 & 0 &  0 & 1 \\
            0 & 0 & 0 &  0 & 0 \\
        \end{bmatrix}
    \end{equation*}
    The pivot columns of $B$, $\{\bfb_1, \bfb_3, \bfb_5\}$ form a basis of its column space.
\end{frame}

\begin{frame}
    \frametitle{Example 9}
    
    Since
    \begin{equation*}
        A = 
        \begin{bmatrix}
            1 & 4 & 0 &  2 & -1\\
            3 & 12 & 1 & 5 & 5 \\
            2 & 8 & 1 &  3 & 2 \\
            5 & 20 & 2 &  8 & 8 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & 4 & 0 &  2 & 0\\
            0 & 0 & 1 & -1 & 0 \\
            0 & 0 & 0 &  0 & 1 \\
            0 & 0 & 0 &  0 & 0 \\
        \end{bmatrix}
        =
        B
        ,
    \end{equation*}
    the columns of $A$, $\{\bfa_1, \bfa_3, \bfa_5\}$ form a basis of its column space.
\end{frame}

\begin{frame}
    \frametitle{Bases of column spaces --- a theorem}
    
    \begin{block}{Theorem 6}
        The pivot columns of a matrix $A$ form a basis for $\colspace A$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Two views of a Basis}

    If $S$ is a basis of $H$,  then it is
    
    \begin{itemize}[<+->]
        \item the \emph{smallest} spanning set 
            --- deleting any vectors in $S$ will make it no longer \emph{span $H$};
        \item the \emph{largest} independent set 
            --- adding any vector of $H$ to $S$ 
            will make it no longer \emph{linearly independent}.
    \end{itemize}
\end{frame}

\end{document}
