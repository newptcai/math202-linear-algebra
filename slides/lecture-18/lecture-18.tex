\input{../meta.tex}

\title{Lecture 18 --- Application to Difference Equations}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{4.8 Application to Difference Equations}

\subsection{Discrete-Time Signals}

\begin{frame}
    \frametitle{Music signal}
    
    The sounds produced from a $CD$ are produced from music that has been sampled
    $44,100$ times per second.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{discrete-time-signal-music.png}
        \caption*{Sampled data from a music signal}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Discrete-Time Signals}
    
    The vector space $\dsS$ (double-indexed sequences) of discrete-time signals contains all function $f: \dsZ
    \mapsto \dsR$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{discrete-time-signal-examples.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Linear Independence in $\dsS$}
    
    Three signals $\bfu = \{u_{k}\}$, $\bfv = \{v_{k}\}$, $\bfw = \{w_{k}\}$ 
    are \alert{linearly independent} if
    \begin{equation*}
        c_{1} \bfu
        +
        c_{2} \bfv
        + 
        c_{2} \bfw
        =
        \bfzero
    \end{equation*}
    i.e.,
    \begin{equation*}
        c_{1} u_{k} 
        +
        c_{2} v_{k} 
        + 
        c_{2} w_{k} 
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ
    \end{equation*}
    implies that $c_{1} = c_{2} = c_{3} =$ \question{}.
\end{frame}

\begin{frame}
    \frametitle{Casorati matrix}

    In other words, for all $k \in \dsZ$,
    \begin{equation*}
        \begin{bmatrix}
            u_{k} & v_{k} & w_{k} \\
            u_{k+1} & v_{k+1} & w_{k+1} \\
            u_{k+2} & v_{k+2} & w_{k+2} \\
        \end{bmatrix}
        \begin{bmatrix}
            c_{1} \\ c_{2} \\ c_{3}
        \end{bmatrix}
        =
        \begin{bmatrix}
            0 \\ 0 \\ 0
        \end{bmatrix}
    \end{equation*}
    The matrix is call \alert{Casorati matrix} of signals.

    If the matrix is \emph{invertible} for some $k$, then the signals are linearly
    independent.

    \pause{}
    
    \emoji{bomb} 
    What can we say if the matrix is \emph{not invertible} for some $k$?
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    
    Are $1^{k}$, $(-2)^{k}$ and $3^{k}$ linearly independent?
\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}
    
    Are $\{k^2\}$ and $\{2 k \abs{k}\}$ linearly independent?

    \hint{} Compute the Casorati matrix for $k = 0, -1, -2$.

    \poll{}
\end{frame}

\subsection{Linear Difference Equations}

\begin{frame}
    \frametitle{A guessing game}
    
    \cake{} Can you guess the next number in the signal $\{f_k\}$
    \begin{equation*}
        \dots, 0, 0, 0, 1, 1, 2, 3, 5, 8, 13, 21, \dots
    \end{equation*}

    \pause{}

    This is the famous \href{https://oeis.org/A000045}{Fibonacci sequence}, i.e.,
    \begin{equation*}
        f_{k} = 
        \begin{cases}
            0 & (k=0) \\
            1 & (k=1) \\
            f_{k-1} + f_{k-2} & \text{otherwise}
        \end{cases}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Linear Difference Equations}
    
    Let $a_{0}, \dots, a_{n}$ be scalars with $a_{0}$ and $a_{n}$ being nonzero,
    and let $\{y_{k}\}, \{z_{k}\}$ be signals.
    The equation
    \begin{equation*}
        a_{0} y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        a_{n} y_{k}
        =
        z_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ.
    \end{equation*}
    is called a \alert{linear difference equation} of \alert{order $n$}.

    \pause{}

    If $z_{k} =0$ for all $k$, then it is call \alert{homogeneous},
    otherwise it is \alert{nonhomogeneous}.

    \pause{}

    The Fibonacci sequence satisfies the following \question{} linear
    difference equation of order \question{}
    \begin{equation*}
        f_{k+2} - f_{k+1} - f_{k} = 0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Find some solutions of
    \begin{equation*}
        y_{k+3} - 2 y_{k+2} - 5 y_{k+1} + 6 y_{k} = 0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    in the form of $r^k$.

    Answer: $3^{k}, 1^k, (-2)^k$.
\end{frame}

\begin{frame}
    \frametitle{Some solutions of homogeneous equations}

    In general, $r^{k}$ is a solution of the equation
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    if $r$ is a root of
    \begin{equation*}
        r^{n}
        +
        a_{1} r^{n-1}
        +
        \dots
        +
        a_{n}
        =
        0
        .
    \end{equation*}

    \bomb{} Does this method give all the solutions?
\end{frame}

\subsection{Solution Sets of Linear Difference Equations}

\begin{frame}
    \frametitle{A vector space}
    
    Let $T:\dsS \mapsto \dsS$ be the mapping that maps $\bfy = \{y_{k}\}$ to
    $\{w_{k}\}$ where
    \begin{equation*}
        w_{k}
        =
        a_{0} y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        a_{n} y_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        .
    \end{equation*}

    \pause{}

    $T$ is a \emph{linear transform} and the solution of
    $T(\bfy) = \bfzero$, i.e.,
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    is the \emph{kernel} of $T$.

    \pause{}

    \emoji{star-struck} So the solution set is \emph{subspace} of $\dsS$.
\end{frame}

\begin{frame}
    \frametitle{Unique solutions}
    
    \begin{block}{Theorem 16}
        If $a_n \ne 0$ and $\{z_{k}\}$ is given, the equation
        \begin{equation*}
            y_{k+n}
            +
            a_{1} y_{k+n-1}
            +
            \dots
            +
            a_{n} y_{k}
            =
            z_{k}
            ,
            \qquad
            \text{for all }
            k \in \dsZ,
        \end{equation*}
        has a unique solution,
        whenever $y_{0}, \dots, y_{n-1}$ are specified.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Dimensions}
    
    \begin{block}{Theorem 17}
        The set $H$ of all solutions of the $n$-th order homogeneous linear difference equation
        \begin{equation*}
            y_{k+n}
            +
            a_{1} y_{k+n-1}
            +
            \dots
            +
            a_{n} y_{k}
            =
            0
            ,
            \qquad
            \text{for all }
            k \in \dsZ,
        \end{equation*}
        is a $n$-dimensional vector space.
    \end{block}

    Proof: Establish an isomorphism between $H$ and $\dsR^{n}$.
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    Find a basis of the solution set of
    \begin{equation*}
        y_{k+3} - 2 y_{k+2} - 5 y_{k+1} + 6 y_{k} = 0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}

    Answer: $\{3^{k}, 1^k, (-2)^k\}$.
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%    
%    \think{} Find \emph{all} solutions of
%    \begin{equation*}
%        f_{k+2} - f_{k+1} - f_{k} = 0
%        ,
%        \qquad
%        \text{for all }
%        k \in \dsZ,
%    \end{equation*}
%\end{frame}

\end{document}
