# # Lecture 01

using LinearAlgebra, RowEchelon

## Solving linear system in Julia
A = [
    1 -2  1; 
    0  2 -8;
    5  0 -5
    ]
x = [0; 8; 10]
A \ x

## Row reduction algorithm

A = [0 3 -6 6 4 -5;
    3 -7 8 -5 8 9;
    3 -9 12 -9 6 15]
rref(A)

## Another example

A = [2 2 5; 
     2 7 -3; 
     -2 -5 -1]
rref(A)