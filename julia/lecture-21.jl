# # Lecture 21
using LinearAlgebra, RowEchelon, SymPy, Random

## For making slides
using Latexify
copy_to_clipboard(true)

# ## Characteristic equations

## Example
@syms λ
A = [
    5 -2 6 -1;
    0 3 -8 0;
    0 0 5 4;
    0 0 0 1;
    ]
B = A - λ*I
charEq = det(B)

## Eigenvalues
@syms λ
charEq = λ^6 - 4* λ^5 - 12* λ^4
charEq1 = factor(charEq)

# ## City-suburb problem

## Find eigenvalues 
A = [.95 .03;
.05 .97]
vals = round.(eigvals(A), digits=3)

## Find and rescale the eigenvectors
vecs = eigvecs(A)
vecs = hcat([v/minimum(v) for v in eachcol(vecs)]...)

## Make x0 a linear combination of eigenvectors
x0 = [0.6; 0.4]
c = round.(vecs \ x0, digits=3)

## The solution
x(k) = vec*((vals).^k .* c)
@syms k
x(k)

# ## Diagnolization

## Power
D = [5 0; 0 3]
D^2
D^3
D^4
@syms k
D^k

## Diagnolize
A = [1 3 3;
    -3 -5 -3;
    3 3 1]
D = diagm(eigvals(A))
P = eigvecs(A)
A1 = round.(P*D*(P^-1))
A == A1

## Diagnolize
A = [2 4 3;
    -4 -6 -3;
    3 3 1]
D = diagm(real.(eigvals(A)))
P = real.(eigvecs(A)) # Not invertible

## Diagnolizable?
A = [5 -8 1;
    0 0 7;
    0 0 -2]
eigvals(A)

## Less than n eigenvalues
A = [5 0 0 0;
    0 5 0 0;
    1 4 -3 0;
    -1 -2 0 -3]
D = diagm(eigvals(A))
P = eigvecs(A)
A1 = round.(P*D*(P^-1))
A == A1

## Less than n eigenvalues
Random.seed!(1234)
A = rand(1:100, (7,7))
A = LowerTriangular(A)
D = diagm(eigvals(A))
P = eigvecs(A)
A1 = round.(P*D*(P^-1))
A == A1

## Exericse
D = [5 0; 0 3]
P = [1 1; -1 -2]
A = P*D*P^-1
D1 = [3 0; 0 5]
P1= [1 1; -2 -1]
A1 = P1*D1*P1^-1
A == A1