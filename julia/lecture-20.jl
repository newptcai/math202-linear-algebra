# # Lecture 20
using LinearAlgebra, RowEchelon, SymPy, Random

## For making slides
using Latexify
copy_to_clipboard(true)

# ## 5.1 Eigenvalues

## Lower trigangular array
A = LowerTriangular(rand(-1:5, (5,5)))
eigvals(A)

## Upper trigangular array
A = UpperTriangular(rand(-1:5, (5,5)))
eigvals(A)

# ## 5.2 Characteristic equations
A = [2 3;
3 -6]
eigvals(A)