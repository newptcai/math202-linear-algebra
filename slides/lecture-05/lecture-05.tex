\input{../meta.tex}

\title{Lecture 05 --- Linear Transformation}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{1.8 Introduction to Linear Transformations}

\begin{frame}
    \frametitle{How to get such a funny picture?}

    \vspace{2em}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{me-2.jpg}
    \end{figure}
\end{frame}

\subsection{Transformations}

\begin{frame}
    \frametitle{Transformations}

    \begin{itemize}[<+->]
        \item A \alert{transformation/function/mapping} $T:\dsR^{n}\mapsto\dsR^{m}$
            is a rule that assigns to each $\bfx \in \dsR^{n}$
            a vector $T(\bfx)$ in $\dsR^{m}$.
        \item $\dsR^{n}$ is the \alert{domain}, and $\dsR^{m}$ is the \alert{codomain}.
        \item $T(\bfx)$ is the \alert{image} of $\bfx$.
        \item The set of all images is the \alert{range} of $T$.
    \end{itemize}

    \pause[\thebeamerpauses]

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{linear-transformation-03.png}
    \end{figure}
\end{frame}

%\begin{frame}[t]
%    \frametitle{Matrix transformations}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{linear-transformation-04.png}
%    \end{figure}
%\end{frame}

%\begin{frame}[t]
%    \frametitle{Projection}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{linear-transformation-05.png}
%        \includegraphics[width=0.4\linewidth]{linear-transformation-06.png}
%    \end{figure}
%
%    \astonished{} An $X$-ray photo is projection of human body!
%\end{frame}

\begin{frame}
    \frametitle{Transformation by multiplication}

    We can think of the product $A \bfx$ as transforming the vector $\bfx$
    to a new vector $A \bfx$.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.9\linewidth]{linear-transformation-01.png}%
        \includegraphics<2>[width=0.6\linewidth]{linear-transformation-02.png}%
    \end{figure}
    \only<1>{\cake{} Let $T(\bfx)=A \bfx$.
    What are the domain, codomain?
    What do we call $\bfb = T(\bfx)$?

    \think{} What is the range?
    }
\end{frame}

\begin{frame}[t]
    \frametitle{Shear transformation}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-07.png}
        \includegraphics[width=\linewidth]{linear-transformation-08.png}
    \end{figure}
\end{frame}

\subsection{Linear Transformations}%

\begin{frame}
    \frametitle{Definition}

    A transformation $T$ is \alert{linear} if
    for all $\bfu$ $\bfv$
    \begin{enumerate}[(i)]
        \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
        \item  $T(c \bfu) = c T(\bfu)$.
    \end{enumerate}

    The matrix transformation $T(\bfx) = A \bfx$ is \emph{linear}.
\end{frame}

\begin{frame}
    \frametitle{Properties}

    If $T$ is a \emph{linear transformation}, then
    for all vectors $\bfu, \bfv$ and scalars $c, d$,
    \begin{enumerate}
        \item[(iii)] $T(\bfzero) = \bfzero$,
        \item[(iv)] $T(c \bfu + d \bfv) = c T (\bfu) + d T(\bfv)$,
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{One more property}
    If $T$ is a \emph{linear transformation},
    then for all vectors $\bfv_1, \cdots, \bfv_{p}$
    and scalars $c_1, \cdots, c_p$.
    \begin{enumerate}
        \item[(v)] $T(c_1 \bfv_1 + \cdots c_{p} \bfv_p)
            = c_1 T(\bfv_1) + \cdots + c_{p} T(\bfv_p)$,
    \end{enumerate}

    \dizzy{} Linear Transformation --- Linear combinations in, linear combinations
    out.
\end{frame}

\begin{frame}
    \frametitle{Example 4 --- Contraction and dilation}

    Let $T(\bfx) = r \bfx$. Show that $T$ is linear.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-10.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 5 --- Rotation}

    Let
    \begin{equation*}
        T(\bfx) = 
        \begin{bmatrix}
            0 & -1 \\
            1 & 0 \\
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ x_2
        \end{bmatrix}
    \end{equation*}
    What is $T(\bfu), T(\bfv)$ and $T(\bfu + \bfv)$ with
    \begin{equation*}
        \bfu = 
        \begin{bmatrix}
            4 \\ 1
        \end{bmatrix}
        ,
        \qquad
        \bfv = 
        \begin{bmatrix}
            2 \\ 3
        \end{bmatrix}
    \end{equation*}

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{linear-transformation-12.png}
    \end{figure}
\end{frame}

\section{1.9 The Matrix of a Linear Transformation}

\subsection{Standard Matrix}

\begin{frame}
    \frametitle{The matrix of a linear transformation}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-13.png}
    \end{figure}

    \pause{}
    
    The example shows that \alert{all} linear mapping $T:\dsR^{2}$ can be written as $T(x) =
    A \bfx$ for some matrix $A$.
\end{frame}

\begin{frame}
    \frametitle{Standard matrix}

    \begin{block}{Theorem 10}
        Let $T : \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation.
        Then
        \begin{equation*}
            T(\bfx) 
            = 
            A
            \bfx
            =
            \begin{bmatrix}
                T(\bfe_1) &
                T(\bfe_2) &
                \cdots &
                T(\bfe_n)
            \end{bmatrix}
            \bfx
        \end{equation*}
        where the matrix $A$ is called the \alert{standard matrix} of $T$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 3 --- Rotation}

    Find the standard matrix $A$ for $T:\dsR^{2} \mapsto \dsR^{2}$
    which rotates a vector by degree $\phi$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{linear-transformation-16.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Geometric linear transformations in $\dsR^{2}$}

    Try this \href{https://observablehq.com/@yurivish/example-of-2d-linear-transforms}{live demonstration}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\linewidth]{linear-transformation-17.png}
    \end{figure}

\end{frame}

\subsection{Existence and Uniqueness Questions}

\begin{frame}
    \frametitle{Onto mapping (existence)}

    $T : \dsR^{n} \mapsto \dsR^{m}$ is \alert{onto} if there is \emph{at least one}
    $\bfx$ such that $T(\bfx) = \bfb$ for all $\bfb \in \dsR^{m}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-20.png}
    \end{figure}

    \pause{}

    If $A$ is the \emph{standard matrix} of $T$, 
    then $A \bfx = \bfb$ is \question{} for all $\bfb \in \dsR^{m}$.

    %\cake{} Which of the following are \emph{onto} mappings?
    %\begin{equation*}
    %    \begin{bmatrix}
    %        1 & 0 & 0 \\
    %        0 & 1 & 0 \\
    %        0 & 0 & 1 \\
    %    \end{bmatrix}
    %    \hspace{3cm}
    %    \begin{bmatrix}
    %        1 & 0 \\
    %        0 & 1 \\
    %        0 & 0 \\
    %    \end{bmatrix}
    %\end{equation*}
\end{frame}

\begin{frame}
    \frametitle{One-to-one mapping (uniqueness)}

    $T : \dsR^{n} \mapsto \dsR^{m}$ is \alert{one-to-one} if there is 
    \alert{at most one} $\bfx$ such that 
    $T(\bfx) = \bfb$ for all $\bfb \in \dsR^{m}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-22.png}
    \end{figure}

    \pause{}

    If $A$ is the \emph{standard matrix} of $T$, 
    then $A \bfx = \bfb$ has \question{} solution(s).

    %\cake{} Which of the following are \emph{onto} mappings?
    %\begin{equation*}
    %    \begin{bmatrix}
    %        1 & 0 & 0 \\
    %        0 & 1 & 0 \\
    %        0 & 0 & 1 \\
    %    \end{bmatrix}
    %    \hspace{3cm}
    %    \begin{bmatrix}
    %        1 & 0 \\
    %        0 & 1 \\
    %        0 & 0 \\
    %    \end{bmatrix}
    %\end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%    
%    \sweat{} Is the following a one-to-one mapping?
%    \begin{equation*}
%        \begin{bmatrix}
%            1 & -4 & 8 & 1 \\
%            0 & 2 & -1 & 3 \\
%            0 & 0 & 0 & 5\\
%        \end{bmatrix}
%    \end{equation*}
%
%    \hint{} Use the following
%    \begin{block}{Theorem 4}
%        The followings are equivalent
%        \begin{enumerate}
%        \item[a.] For each $\bfb$, the equation $A \bfx = \bfb$ has a solution, i.e., it is
%            \emph{consistent}.
%        \item[d.] $A$ has pivot position in every row.
%        \end{enumerate}
%    \end{block}
%\end{frame}

\begin{frame}
    \frametitle{When is $T$ one-to-one?}
    
    \begin{block}{Theorem 11}
        Let $T: \dsR^{n} \mapsto \dsR^{m}$ be a linear transformation.
        $T$ is \emph{one-to-one} if and only if $T(\bfx) = \bfzero$ 
        has only the trivial solution.
    \end{block}
\end{frame}

%\begin{frame}
%    \frametitle{When is $T$ one-to-one?}
%
%    %\think{} Can you prove it using the following?
%
%    %\begin{block}{Theorem 11}
%    %    $T$ is \emph{one-to-one} if and only if $T(\bfzero) = \bfzero$ 
%    %    has only the trivial solution.
%    %\end{block}
%\end{frame}

\begin{frame}
    \frametitle{When is $T$ onto?}
    
    \begin{block}{Theorem 12}
        Let $T: \dsR^{m} \mapsto\dsR^{n}$ be a linear transformation, 
        and let $A = [\bfa_{1} \, \bfa_{2} \, \dots \, \bfa_{n}]$ be its standard matrix.

        (a) Then $T$ is onto if and only if $\Span{\bfa_{1}, \dots, \bfa_{n}} = \dsR^{m}$.

        \pause{}

        (b) Then $T$ is one-to-one if and only if $\bfa_{1}, \dots, \bfa_{n}$ are \emph{linearly
        independent}.
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{Example 5}
    
    \think{} Is $T$ one-to-one? Is $T$ onto? 

    \begin{equation*}
        T\left(
            \begin{bmatrix}
                x_1 \\ x_2
            \end{bmatrix}
        \right)
        =
        \begin{bmatrix}
            3 x_1 + x_2 \\
            5 x_1 + 7 x_2 \\
            x_1 + 3 x_2
        \end{bmatrix}
    \end{equation*}

    \pause{}

    \begin{block}{\hint{} Theorem 4}
        If $A$ is a $m \times n$ matrix, with columns
        $\bfa_1,\dots, \bfa_n$, and $\bfx \in \dsR^n$, then
        the followings are equivalent
        \begin{enumerate}
        \item[c.] $\dsR^{m} = \Span{\bfa_1, \dots, \bfa_n}$.
        \item[d.] $A$ has a pivot position in every row.
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Think-Pair-Share \think{}}

    (a) Are these two mappings linear? Why or why not?
    \begin{equation*}
        T_{1}
        \left(
            \begin{bmatrix}
                x_1 \\ x_2  \\ x_3
            \end{bmatrix}
        \right) =
        \begin{bmatrix}
                x_1 \\ x_2  \\ -x_3
        \end{bmatrix}
        ,
        \qquad
        T_{2}
        \left(
            \begin{bmatrix}
                x_1 \\ x_2
            \end{bmatrix}
        \right) =
        \begin{bmatrix}
            4 x_1 -2 x_2 \\ 3 \abs{x_2}
        \end{bmatrix}
        ,
    \end{equation*}

    \pause{}

    (b) Find the standard matrix $A$ for $T(\bfx) = r \bfx$.

    \pause{}

    (c) Which matrix transforms the left to the right?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.33\linewidth]{me.jpg}
        \includegraphics[width=0.33\linewidth]{me-1.jpg}
    \end{figure}
\end{frame}

\end{document}
