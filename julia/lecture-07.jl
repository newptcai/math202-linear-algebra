# # Lecture 07

using LinearAlgebra, RowEchelon

## For making slides
using Latexify
copy_to_clipboard(true)

## non-singular matrix
A = [2 5; -3 -7]
inv(A)

## Theorem 4
A = [3 4; 5 6]
inv(A)

## determinant
det(A)

## singular matrix
A = [3 2; 9 6]
det(A)

## try to invert a singular matrix cause an ERROR
inv(A)

## Theorem 5
A = [2 5; -3 -7]
b = [3; 7]
inv(A) * b

## The algorithm
A = [ 0 1 2; 1 0 3; 4 -3 8]
A1= hcat(A, I)
println("The augmented matrx")
display(A1)
println("The reduced echelon form")
A2 = round.(Int64, rref(A1))
display(A2)
println("The inverse of A")
display(A2[:, 4:6])

## Guess and prove
As = [LowerTriangular(ones(i,i)) for i in 1:5]
println("The inverses")
for A in As
    display(inv(A))
end

## Elementary matrix
E = [1 0 0;
    0 1 0;
    -4 0 1]
F = [1 0 0;
    0 1 0;
    4 0 1]
E*F
F*E