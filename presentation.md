---
title: Presentations Arrangement
subtitle: MATH 202 Linear Algebra
colorlinks: true
urlcolor: Maroon
marings: 1.5in
...

As part of the evaluation process of this course,
you will give a 10 minutes presentation
on a topic related to linear algebra.

# Topic

You should choose a topic which

1. has not been covered in the class at the time of your presentation,
2. and is suitable for the audience of your fellow students.

Here are some possible source of topics

1. Applications listed in the slides of Lecture 1.
2. Our textbook which have been skipped.
3. [When Life is Linear Tim Chartier](https://www.jstor.org/stable/10.4169/j.ctt19b9kd8?turn_away=true).

# Format

The presentations will be held on Fridays of week 4-7.
(Exact time to be decided by polling.)
You will be randomly assigned to one of these 4 weeks to present.
Except for the week when you present,
you do *not* need to attend presentation meetings.

You can attend either remotely or in-person.
You can also choose to do a slide-show or give a whiteboard presentation.

The presentations will be recorded for grading purpose and for other students to
watch.

# Exemplary talks

Here are [links](https://duke.box.com/s/lousc9o14adugoin2yu1jgjc2kye8wk6) to 
some excellent student presentations from another course.

\pagebreak{}

# Rubrics

Your presentation will be graded as follows.

## Excellent (5pt)

1. The topic is neither too difficult nor too easy for the audience.
2. Have a set of well-prepared slides or notes.
3. Speak English fluently. Explain the topic clearly. Have a good motivation for the
   problem/application being presented. Have a sense of humour.
4. Finish the talk in about 10 minutes.
5. Well-prepared for questions from the audience.
6. Ask other students relevant questions about their presentations during a
   presentation meeting.

## Good (4pt)

1. The topic is suitable, but too difficult.
2. Have a set of well-prepared slides or notes.
3. Speak English mostly fluently. Explain the topic more or less clearly. Have a
   motivation for the problem/application being presented.
4. Finish the talk in about 10 minutes.
5. Can respond to questions from the audience in a reasonable way.
6. Ask other students questions about their presentations during a presentation
   meeting.

## Fine (3pt)

1. The topic is suitable, but too easy.
2. Have a set of reasonably-prepared slides or notes.
3. Can explain the topic in English. 
4. Finish the talk in about 10 minutes.
5. Can respond to questions from the audience.

## Pass (2pt)

1. The topic is not very suitable.
2. Have a set of slides or notes.
3. Have some trouble explaining the topic in English. 
4. Finish the talk in much less than 10 minutes.
5. Have trouble respond to questions from the audience.

## Complete (1pt)

1. The presentation has been given.

## Incomplete (0pt)

1. The presentation has not been given.
