\input{../meta.tex}

\title{Lecture 25 --- Orthogonal Projections, The Gram-Schmidt Process}

\setbeamertemplate{footline}[text line]{%
\parbox{\linewidth}{\footnotesize\vspace*{-22pt}\url{https://duke.evaluationkit.com/} \emoji{pray}\hfill\insertpagenumber}}
%\setbeamertemplate{navigation symbols}{}

\begin{document}

\maketitle{}

\section{6.3 Orthogonal Projections}

\begin{frame}
    \frametitle{Projections to a subspace in $\dsR^{n}$}

    Given $\bfy \in \dsR^{n}$ and subspace $W$ of $\dsR^{n}$, we want to find
    $\bfy = \bfz + \hat{\bfy}$, such that $\bfz \in W^{\perp}$ and $\hat{\bfy} \in W$.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{projection.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    Let $\{\bfu_{1}, \dots, \bfu_{5}\}$ be an orthogonal basis for $\dsR^{5}$.
    Let
    \begin{equation*}
        \bfy = 
        c_{1} \bfu_1
        +
        c_{2} \bfu_2
        +
        c_{3} \bfu_3
        +
        c_{4} \bfu_4
        +
        c_{5} \bfu_5
        .
    \end{equation*}
    Let $W = \Span{\bfu_1, \bfu_2}$.

    What are $\bfz$ and $\hat{\bfy}$?
\end{frame}

\begin{frame}
    \frametitle{The Orthogonal Decomposition Theorem}
    
    \begin{block}{Theorem 8}
        The projection of $\bfy$ to $W$ is unique.
        If $\{\bfu_{1}, \dots, \bfu_{p}\}$ is an orthogonal basis of $W$,
        then
        \begin{equation*}
            \hat{\bfy} = 
            \frac{\bfy \cdot \bfu_1}{\bfu_1 \cdot \bfu_1} \bfu_1
            +
            \frac{\bfy \cdot \bfu_2}{\bfu_2 \cdot \bfu_2} \bfu_2
            +
            \cdots
            +
            \frac{\bfy \cdot \bfu_p}{\bfu_p \cdot \bfu_p} \bfu_p
            ,
        \end{equation*}
        and
        \begin{equation*}
            \bfz = \bfy - \hat{\bfy}.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    
    Let
    \begin{equation*}
        \bfu_1
        =
        \left[
            \begin{array}{c}
                2 \\
                5 \\
                -1 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfu_2
        =
        \left[
            \begin{array}{c}
                -2 \\
                1 \\
                1 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfy
        =
        \left[
            \begin{array}{c}
                1 \\
                2 \\
                3 \\
            \end{array}
        \right]
        .
    \end{equation*}
    Let $W = \Span{\bfu_1, \bfu_2}$.

    What are $\hat{\bfy}$ and $\bfz$?
\end{frame}

\begin{frame}
    \frametitle{A Geometric Interpretation}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{projection-r3.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Best Approximation Theorem}
    
    \begin{block}{Theorem 9}
        Let $\hat{\bfy} = \proj_W \bfy$.
        Then for all $\bfv \in W$ with $\bfv \ne \hat{\bfy}$,
        \begin{equation*}
            \norm{\bfy - \hat{\bfy}}
            \alert{<}
            \norm{\bfy - \bfv}
            .
        \end{equation*}
    \end{block}

    \begin{figure}[htpb]
        \centering
        \includegraphics<2>[width=0.7\linewidth]{projection-best-approximation.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Orthonormal bases}
    
    \begin{block}{Theorem 10}
        If $\{\bfu_{1},\dots, \bfu_{p}\}$ is an orthonormal basis of $W$, then
        \begin{equation*}
            \proj_{W} \bfy 
            =
            (\bfy \cdot \bfu_{1})
            \bfu_{1}
            +
            (\bfy \cdot \bfu_{2})
            \bfu_{2}
            +
            \cdots
            (\bfy \cdot \bfu_{p})
            \bfu_{p}
            .
        \end{equation*}
        \pause{}
        If $U = \begin{bmatrix}\bfu_{1} & \cdots & \bfu_{p} \end{bmatrix}$, then
        \begin{equation*}
            \proj_{W} \bfy
            =
            U U^{T} \bfy
            .
        \end{equation*}
    \end{block}
\end{frame}

\section{6.4 The Gram-Schmidt Process}

\begin{frame}
    \frametitle{Example 1}
    
    \begin{columns}
        \begin{column}{0.5\textwidth}
            Let $W = \Span{\bfx_1, \bfx_2}$ where
            \begin{equation*}
                \bfx_1
                =
                \begin{bmatrix}
                    3 \\ 6 \\ 0
                \end{bmatrix}
                ,
                \quad
                \bfx_2
                =
                \begin{bmatrix}
                    1 \\ 2 \\ 2
                \end{bmatrix}
            \end{equation*}

            Find an orthogonal basis of $W$.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.4-fig-1.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            Let $W = \Span{\bfx_1, \bfx_2, \bfx_3}$ where
            \begin{equation*}
                \bfx_1
                =
                \begin{bmatrix}
                    1 \\ 1 \\ 1 \\ 1
                \end{bmatrix}
                ,
                \quad
                \bfx_2
                =
                \begin{bmatrix}
                    0 \\ 1 \\ 1 \\ 1
                \end{bmatrix}
                ,
                \quad
                \bfx_3
                =
                \begin{bmatrix}
                    0 \\ 0 \\ 1 \\ 1
                \end{bmatrix}
                .
            \end{equation*}
            Find an orthogonal basis of $W$.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.4-fig-2.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{The Gram-Schmidt Process}
    
    \begin{block}{Theorem 11}
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=\linewidth]{gram-schmidt.png}
        \end{figure}
        \vspace{-2em}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 3 -- Orthonormal bases}
    
    Construct an orthonormal basis form the orthogonal basis
    \begin{equation*}
        \bfv_{1}
        =
        \begin{bmatrix}
            3 \\ 6 \\ 0
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2}
        =
        \begin{bmatrix}
            0 \\ 0 \\ 2
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{QR Factorization}
    
    \begin{block}{Theorem 12}
    If $A$ is an $m \times n$ matrix with linearly independent columns, 
    then $A$ can be factored as $A = QR$, 
    \begin{itemize}
        \item where $Q$ is an $m \times n$ matrix whose columns form an orthonormal basis for $\colspace A$ 
        \item and $R$ is an $n \times n$ upper triangular invertible matrix with positive entries on its diagonal.
    \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 4}
    
    Let
    \begin{equation*}
        A
        =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                1 & 1 & 0 \\
                1 & 1 & 1 \\
                1 & 1 & 1 \\
            \end{array}
        \right]
    \end{equation*}

    Find a QR factorization for $A$.
\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}
    Suppose $A = QR$, where $Q$ is $m \times n$ and $R$ is $n \times n$. 
    Show that if the columns of $A$ are linearly independent, 
    then $R$ must be invertible. 

    \hint{} Why does equation $R \bfx = 0$ have only the trivial solution?
\end{frame}

\end{document}
