\input{../meta.tex}

\title{Lecture 24 --- Orthogonal Sets,  Orthogonal Projections}

\begin{document}

\maketitle{}

\section{6.2 Orthogonal Sets}

\begin{frame}[c]
    \frametitle{Orthogonal sets}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            A set of vectors $\{\bfu_1, \dots, \bfu_p\}$ in $\dsR^{n}$ 
            is an \alert{orthogonal set} if $\bfu_i \cdot \bfu_j = 0$
            for all $i \ne j$.

            For example,
            \begin{equation*}
                \bfu_{1} =
                \begin{bmatrix}
                    3 \\ 1 \\ 1
                \end{bmatrix}
                ,
                \quad
                \bfu_{2} =
                \begin{bmatrix}
                    -1 \\ 2 \\ 1
                \end{bmatrix}
                ,
                \quad
                \bfu_{3} = 
                \begin{bmatrix}
                    \frac{-1}{2} \\ -2 \\ \frac{7}{2}
                \end{bmatrix}
            \end{equation*}
            form an orthogonal set.
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{6.2-01.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Bases}
    
    \begin{block}{Theorem 4}
        If $S = \{\bfu_1, \dots, \bfu_p\}$ is an orthogonal set of \emph{non-zero}
        vectors,
        then the vectors in $S$ are linearly independent and hence forms a basis for
        the subspace spanned by $S$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Orthogonal basis}
    
    An \alert{orthogonal basis} for a subspace $W$ of $\dsR^n$ is a basis for $W$
    that is also an orthogonal set.

    \begin{block}{Theorem 5}
        Let $\{\bfu_1, \dots, \bfu_p\}$ be an orthogonal basis of $W$,
        then for all $\bfy \in W$, the weights in
        \begin{equation*}
            \bfy = c_{1} \bfu_1 + \dots c_p \bfu_p
        \end{equation*}
        satisfies that for all $j$
        \begin{equation*}
            c_{j} = \frac{\bfy \cdot \bfu_{j}}{\bfu_{j}\cdot \bfu_j}.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    
    Express
    \begin{equation*}
        \bfy = 
        \begin{bmatrix}
            6 \\ 1 \\ -8
        \end{bmatrix}
    \end{equation*}
    as a linear combination of the orthogonal basis
    \begin{equation*}
        \bfu_{1} =
        \begin{bmatrix}
            3 \\ 1 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfu_{2} =
        \begin{bmatrix}
            -1 \\ 2 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfu_{3} = 
        \begin{bmatrix}
            \frac{-1}{2} \\ -2 \\ \frac{7}{2}
        \end{bmatrix}
    \end{equation*}
    forms an orthogonal set.
\end{frame}

\begin{frame}
    \frametitle{Projections to a vector}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{projection-2d.png}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \pause{}
            Give $\bfu, \bfy \in \dsR^{n}$, we want to write
            \begin{equation*}
                \bfy = \bfz + \hat{\bfy}
            \end{equation*}
            such that $\bfz$ is orthogonal to $\bfu$
            and $\hat{\bfy} = \alpha \bfu$ for some $\alpha \in \dsR$.

            \pause{}
            $\hat{\bfy}$ is called the \alert{orthogonal projection/projection} of $\bfy$ onto $\bfu$, 
            and $\bfz$ is called the \alert{component of $\bfy$ orthogonal to
            $\bfu$}.
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{Compute the projection}
    
    We have
    \begin{equation*}
        \hat{\bfy} 
        = 
        \frac{\bfy \cdot \bfu}{\bfu \cdot \bfu}
        \bfu
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Projections to a subspace}
    For any $c \ne 0$, the projection of $\bfy$ to $c \bfu$ is the same.

    Thus for the subspace $L = \Span{\bfu}$, and we define
    \begin{equation*}
        \proj_{L} \bfy =
        \frac{\bfy \cdot \bfu}{\bfu \cdot \bfu}
        \bfu
        .
    \end{equation*}

    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{projection-subspace.png}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Projection}
%    
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.8\linewidth]{projection.png}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Example 3}
    
    Find the orthogonal projection of $\bfy$ to $\bfu$ where
    \begin{equation*}
        \bfy = 
        \begin{bmatrix}
            7 \\ 6
        \end{bmatrix}
        ,
        \qquad
        \bfu =
        \begin{bmatrix}
            4 \\ 2
        \end{bmatrix}
    \end{equation*}

    \pause{}

    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{projection-subspace.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A geometric interpretation}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{block}{Theorem 5}
                Let $\{\bfu_1, \dots, \bfu_p\}$ be an orthogonal basis of $W$,
                then for all $\bfy \in W$, the weights in
                \begin{equation*}
                    \bfy = c_{1} \bfu_1 + \dots c_p \bfu_p
                \end{equation*}
                satisfies that for all $j$
                \begin{equation*}
                    c_{j} = \frac{y \cdot \bfu_{j}}{\bfu_{j}\cdot \bfu_j}.
                \end{equation*}
            \end{block}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{projection-r2.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Orthonormal Sets}

    A set $\{\bfu_{1},\dots,\bfu_{p}\}$ is an \alert{orthonormal set} if it is an \alert{orthogonal set of unit vectors}.

    For example, the following vectors form an \alert{orthonormal set} ---
    \begin{equation*}
        \bfv_{1}
        =
        \frac{1}{\sqrt{11}}
        \begin{bmatrix}
            3 \\
            1 \\
            1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2}
        =
        \frac{1}{\sqrt{6}}
        \begin{bmatrix}
            -1 \\
            2 \\
            1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfv_{3}
        =
        \frac{1}{\sqrt{66}}
        \begin{bmatrix}
            -1 \\
            -4 \\
            7 \\
        \end{bmatrix}
    \end{equation*}
    
    %\begin{columns}
    %    \begin{column}{0.3\textwidth}
    %        \begin{figure}[htpb]
    %            \centering
    %            \includegraphics[width=\linewidth]{projection-orthonormal.png}
    %        \end{figure}
    %    \end{column}
    %    \begin{column}{0.7\textwidth}
    %        
    %    \end{column}
    %\end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 6}
    
    Let
    \begin{equation*}
        U
        =
        \left[
            \begin{array}{cc}
                \frac{\sqrt{2}}{2} & \frac{2}{3} \\
                \frac{\sqrt{2}}{2} & \frac{-2}{3} \\
                0 & \frac{1}{3} \\
            \end{array}
        \right]
        ,
        \qquad
        \bfx
        =
        \left[
            \begin{array}{c}
                \sqrt{2} \\
                3 \\
            \end{array}
        \right]
        .
    \end{equation*}
    Note that $U$ has \alert{orthonormal columns}.

    \pause{}

    What are $U^T U$, $\norm{U \bfx}$ and $\norm{\bfx}$?
\end{frame}

\begin{frame}
    \frametitle{Orthonormal columns}
    
    \begin{block}{Theorem 6}
        An $m \times n$ matrix $U$ has orthonormal columns if and only if $U^T U =
        \text{\question}$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Orthonormal columns}

    \begin{block}{Theorem 7}
    Let $U$ be an $m \times n$ matrix with orthonormal columns, 
    and let $x$ and $y$ be in $\dsR^n$.
    Then
    \begin{enumerate}[a.]
        \item $\norm{U \bfx} = \norm{\bfx}$.
        \item $(U \bfx) \cdot (U \bfy) = \bfx \cdot \bfy$.
        \item $(U \bfx) \cdot (U \bfy) = 0$ if and only if $\bfx \cdot \bfy = 0$.
    \end{enumerate}
    \end{block}
    \cake{} Why does (b) imply (a)?
\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}

    \begin{block}{Theorem 7}
    Let $U$ be an $m \times n$ matrix with orthonormal columns, 
    and let $x$ and $y$ be in $\dsR^n$.
    Then
    \begin{enumerate}
        \item[b.] $(U \bfx) \cdot (U \bfy) = \bfx \cdot \bfy$.
    \end{enumerate}
    \end{block}

    \pause{}

    \hint{} Use the following and the definition of dot product.
    \begin{block}{Theorem 6}
        An $m \times n$ matrix $U$ has orthonormal columns if and only if $U^T U =
        I$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Orthogonal matrix}

    An \alert{orthogonal matrix} is a square matrix $U$ with $U^{-1} = U^{T}$.
    Such a matrix has orthonormal columns.

    \begin{block}{Theorem 6}
        An $m \times n$ matrix $U$ has orthonormal columns if and only if $U^T U = I$.
    \end{block}

    \pause{}

    \cake{} Is every matrix with \emph{orthonormal columns} 
    a \emph{orthogonal matrix}?
\end{frame}

\end{document}
