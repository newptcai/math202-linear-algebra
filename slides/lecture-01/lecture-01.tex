\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 01 --- Systems of Linear Equations, Echelon Forms}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{Why Linear Algebra?}

\begin{frame}
    \frametitle{Because of Machine Learning}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{machine_learning_2x.png}
        \caption*{\url{https://xkcd.com/1838/}}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{And because it has many other applications}

    Linear Algebra provides a \emph{concise language} to describe how to

    \begin{itemize}
        \item \emoji{bomb} maximize war effort
            (\href{https://en.wikipedia.org/wiki/Linear\_programming\#History}{linear
            programming}),
        \item \emoji{lock} build ``unbreakable'' cipher code
            (\href{https://www.math.utah.edu/~gustafso/s2016/2270/published-projects-2016/adamsTyler-moodyDavid-choiHaysun-CryptographyTheEnigmaMachine.pdf}{cryptography}),
        \item
            \emoji{robot} make computers ``smarter''
            (\href{https://www.analyticsvidhya.com/blog/2019/07/10-applications-linear-algebra-data-science/}{artificial intelligence}),
        \item \emoji{framed-picture} create funny photos
            (\href{https://observablehq.com/@yurivish/example-of-2d-linear-transforms}{linear
            transformation}),
        \item \emoji{spider-web} process complex network
            (\href{https://en.wikipedia.org/wiki/GraphBLAS}{GraphBLAS}),
        \item \emoji{car} control the Mars rover 
            (\href{https://en.wikipedia.org/wiki/Linear\_network\_coding}{linear
            network coding}),
        \item \emoji{notes} recommend music 
            (\href{https://en.wikipedia.org/wiki/Singular\_value\_decomposition}{SVD}),
        \item \ldots
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{An example --- 2D convolution}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{imageProcess1.png}
        \includegraphics[width=0.6\linewidth]{stride1-300x107.png}
        \caption*{Pictures by \href{https://www.analyticsvidhya.com/blog/2019/07/10-applications-linear-algebra-data-science/}{Khyati Mahendru}}%
    \end{figure}

    See this
    \href{https://magamig.github.io/posts/real-time-video-convolution-using-webgl/}{live
        demonstration}.
\end{frame}

\section{1.1 Systems of Linear Equations}%

\subsection{Linear Systems}%

\begin{frame}
    \frametitle{Linear equations}
    
    A \alert{linear equation} in the variables $x_{1}, \ldots, x_{n}$ is an equation like
    \begin{equation*}
        a_{1} x_{1} + a_{2} x_{2} + \dots + a_{n} x_{n} = b.
    \end{equation*}

    \cake{} Which of the followings are \emph{not} linear equations?
    \begin{center}
        \begin{minipage}{0.4\textwidth}
            \begin{enumerate}
                \item $4 x_1 - 5 x_2 + 2 = x_1$
                \item $x_2 = 2(\sqrt{6} - x_{1})+x_3$
                \item $x_2 = 2 (\sqrt{x_1} - 6)$
                \item $2 x_1 + x_2 - x_3 = 2 \sqrt{6}$
                \item $4 x_1 - 5 x_2 = x_1 x_2$
            \end{enumerate}
        \end{minipage}
    \end{center}
\end{frame}

\begin{frame}[t]
    \frametitle{Linear Systems}
    
    A \alert{system of linear equations (linear system)} is group of linear equations
    using the same variables, e.g.,
    \begin{equation}
        \label{eq:sys}
        \systeme{2 x_1 - x_2 + 1.5 x_3 = 8,
                   x_1        -  4 x_3   = -7}
    \end{equation}
    
    A \alert{solution} of a linear system is a list of numbers $(s_1, s_{2}, \dots,
    s_{n})$ such that replacing $x_i$ by $s_i$ makes all the equations true.

    One solution of \eqref{eq:sys} is $(5, 6.5, 3)$.
\end{frame}

\begin{frame}
    \frametitle{Solution sets}
    The set of all solutions of a linear system is the \alert{solution set}.

    Two systems are \alert{equivalent} if their solution sets are the same.

    Consider
    \begin{equation*}
        \systeme{2 x_1 - x_2 + 1.5 x_3 = 8, 
        x_1 - 4 x_3= -7}
    \end{equation*}
    and
    \begin{equation*}
       \systeme{4 x_1 - 2 x_2 + 3 x_3 = 16,
         - 3 x_1 + 12 x_3  = 21}
    \end{equation*}

    \cake{} Are these two systems equivalent?
\end{frame}

\subsection{Three Types of Solution Sets}

\begin{frame}[c]
    \frametitle{Three cases}
    
    The \emph{fundamental question} about a linear systems is that if it is
    \begin{itemize}
        \item \alert{inconsistent},  i.e., having no solutions, or
        \item \alert{consistent}, i.e., having 
            \begin{itemize}
                \item one \alert{unique solution}, or
                \item \alert{infinitely many solutions}.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Unique solutions}
    
    The following linear system is \emph{consistent} and has a \emph{unique solution}
    \begin{equation*}
        \systeme{x_1 - 2 x_2 = -1,
        - x_1 + 3 x_2 = 3}
    \end{equation*}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{3-case-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Non-unique solutions}

    System (a) is \question{} and has \question{} solutions?

    System (b) is \question{} and has \question{} solutions?
    \begin{equation*}
        (a) \systeme{x_1 - 2 x_2 = -1,
        - x_1 + 2 x_2 = 3}
        \quad
        (b) \systeme{x_1 - 2 x_2 = -1,
        - x_1 + 2 x_2 =  1}
    \end{equation*}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=1.0\linewidth]{3-case-2.png}
    \end{figure}
\end{frame}

\subsection{Matrix Notations}%

\begin{frame}[c]
    \frametitle{What is a matrix?}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}

            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.7\linewidth]{matrix.jpg}
            \end{figure}
            
        \end{column}
        \begin{column}{0.5\textwidth}
            A \alert{matrix} is rectangular array of numbers like this
            \begin{equation*}
            \begin{bmatrix}
                1 & -2 & 1 \\
                0 &  2 & -8 \\
                5 &  0 & -5 \\
            \end{bmatrix}
            \end{equation*}
            It has nothing to do with the film Matrix \emoji{joy}.
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Matrix notations for linear systems}
    
    Given a system
    \begin{equation*}
        \systeme{x_1 - 2 x_2 + x_3 = 0,
            2 x_2 - 8 x_3 = 8,
            5 x_1 - 5 x_3 = 10}
    \end{equation*}
    the \alert{coefficient matrix} 
    and
    the \alert{augmented matrix} are
    \begin{equation*}
            \begin{bmatrix}
                1 & - 2 &    1 \\
                0 &   2 &   -8 \\
                5 &   0 &   -5
            \end{bmatrix}
        \hspace{2cm}
            \begin{bmatrix}
                1 & - 2 &    1  & 0 \\
                0 &   2 &   -8  & 8 \\
                5 &   0 &   -5  & 10
            \end{bmatrix}
    \end{equation*}
\end{frame}

\subsection{Solving Linear Systems}%

\begin{frame}[c]
    \frametitle{Ways to solve linear system}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            To solve
            \begin{equation}
                \label{eq:sys:1}
                \systeme{x_1 - 2 x_2 +   x_3 = 0,
                2 x_2 - 8 x_3 = 8,
                5 x_1 - 5 x_3 = 10}
            \end{equation}
            you can
            \begin{itemize}
                \item \emoji{cry} and give up,
                \item \emoji{phone} you mum \emoji{old-woman} for help, or
                \item draw some pictures \emoji{artist}, or
                \item use a computer \emoji{robot}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{3-plane.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{How the textbook does it}
    
    Alternatively, you can show that the following are equivalent
    \begin{equation*}
        \systeme{x_1 - 2 x_2 +   x_3 = 0,
        2 x_2 - 8 x_3 = 8,
        5 x_1 - 5 x_3 = 10}
        \hspace{2cm}
        \systeme{x_1 = 1,
        x_2 = 0,
        x_3 = -1}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Row operations}
    
    The three types of \alert{row operations} on a matrix are
    \begin{itemize}
        \item \alert{Replace} one row by the sum of itself and a multiple of another row.
        \item \alert{Interchange} two rows.
        \item \alert{Scale} a row by multiplying it with a \emph{non-zero} constant.
    \end{itemize}
    We solved \eqref{eq:sys:1} by applying row operations to the left (the augmented
    matrix) 
    to get the right.
    \begin{equation*}
        \begin{bmatrix}
            1 & - 2 &    1  & 0 \\
            0 &   2 &   -8  & 8 \\
            5 &   0 &   -5  & 10
        \end{bmatrix}
        \hspace{2cm}
        \begin{bmatrix}
            1 & 0 & 0  & 1 \\
            0 & 1 & 0  & 0 \\
            0 & 0 & 1  & -1
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{Exercise \think{}}

    Can you transfer the left to the right with three row operations?
    \begin{equation*}
        \begin{bmatrix}
            1 & 2 & -5 & 0 \\
            0 & 1 & -3 & -2 \\
            0 & -3 & 9 & 5 \\
        \end{bmatrix}
        \hspace{2cm}
        \begin{bmatrix}
            2 & 4 & -10 & 0 \\
            0 & 0 & 0 & 1 \\
            0 & 1 & -3 & -2 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\section{1.2 Row Reduction and Echelon Forms}%

\subsection{Echelon and Reduced Echelon Forms}

\begin{frame}
    \frametitle{Echelon formations}

    \vspace{2em}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{Echelon-formation.png}
        \caption*{Echelon formation of troops and ships}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Echelon and reduced echelon form}

    $\filledsquare$ represents a \emph{non-zero} number,
    \textasteriskcentered{} represents \emph{any} number.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{echlon-1.png}
        \caption*{Matrices of echelon form}%
    \end{figure}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{echlon-2.png}
        \caption*{Matrices of reduced echelon form}%
    \end{figure}

    The positions of leading $\filledsquare$ are \alert{pivot positions}.
    The columns containing these positions are \alert{pivot columns}.
\end{frame}

\begin{frame}
    \frametitle{Exercise \think{}}

    Are these in echelon form? Reduced echelon form?
    \begin{equation*}
        \begin{bmatrix}
            1 & - 2 &    1  & 0 \\
            2 &   2 &   -8  & 8 \\
            0 &   0 &   -5  & 10 \\
        \end{bmatrix}
        \hspace{1cm}
        \begin{bmatrix}
            1 & - 2 &    1  & 0 \\
            0 &   2 &   -8  & 8 \\
            0 &   0 &   -5  & 10 \\
        \end{bmatrix}
        \hspace{1cm}
        \begin{bmatrix}
            1 & - 2 &    1  & 0 \\
            0 &   0 &    0  & 0 \\
            0 &   2 &   -8  & 8 \\
        \end{bmatrix}
    \end{equation*}
    \begin{equation*}
        \begin{bmatrix}
            1 &   0 &    0  & 0 \\
            0 &   1 &    0  & 4 \\
            0 &   0 &    2 & 2
        \end{bmatrix}
        \hspace{1cm}
        \begin{bmatrix}
            1 &   0 &    0  & 0 \\
            0 &   1 &    0  & 4 \\
            0 &   0 &   1 & 2
        \end{bmatrix}
        \hspace{1cm}
        \begin{bmatrix}
            1 &   0 &    0  & 0 \\
            0 &   1 &    2  & 4 \\
            0 &   0 &    1 & 2
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{To the reduced echelon form}
    
    Row reduce the following matrix to its reduced echelon form
    \begin{equation*}
        \begin{bmatrix}
            0 & -3 & -6 \\
            -1 & -2 & -1 \\
            -2 & -3 & 0
        \end{bmatrix}
        \rightarrow
        \begin{bmatrix}
            -1 & -2 & -1 \\
            0 & -3 & -6 \\
            -2 & -3 & 0
        \end{bmatrix}
        \rightarrow
        \dots
    \end{equation*}
\end{frame}

\subsection{The Algorithm to Get Reduced Echelon Forms}

\begin{frame}
    \frametitle{The algorithm}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \only<1->{Begin with the leftmost nonzero column.
                \begin{equation*}
                    \begin{bmatrix}
                        0 & 3 & -6 & 6 & 4 & -5 \\
                        3 & -7 & 8 & -5 & 8 & 9 \\
                        3 & -9 & 12 & -9 & 6 & 15 \\
                    \end{bmatrix}
            \end{equation*}}

            \only<2->{Select a nonzero entry in the pivot column as a pivot.
                \begin{equation*}
                    \begin{bmatrix}
                        3 & -9 & 12 & -9 & 6 & 15 \\
                        3 & -7 & 8 & -5 & 8 & 9 \\
                        0 & 3 & -6 & 6 & 4 & -5 \\
                    \end{bmatrix}
            \end{equation*}}

        \end{column}
        \begin{column}{0.5\textwidth}
            \only<3->{Use row replacement operations to create zeros.
                \begin{equation*}
                    \begin{bmatrix}
                        3 & -9 & 12 & -9 & 6 & 15 \\
                        0 & 2 & -4 & 4 & 2 & -6 \\
                        0 & 3 & -6 & 6 & 4 & -5 \\
                    \end{bmatrix}
            \end{equation*}}

            \only<4>{Ignore the first row, choose the next pivotal column, repeat.
                \begin{equation*}
                    \begin{bmatrix}
                        \color{gray}3 & \color{gray}-9 & \color{gray}12 & \color{gray}-9 & \color{gray}6 & \color{gray}15 \\
                        0 & 2 & -4 & 4 & 2 & -6 \\
                        0 & 3 & -6 & 6 & 4 & -5 \\
                    \end{bmatrix}
            \end{equation*}}

        \end{column}
    \end{columns}

\end{frame}

\begin{frame}[c]
    \frametitle{Uniqueness of reduced echelon form}

    \begin{block}{Theorem 1}
        Each matrix is row equivalent to one and only one reduced echelon matrix.
    \end{block}

    See Appendix A of the textbook or \href{https://bit.ly/3DVu33N}{here} for a proof.
\end{frame}

\begin{frame}[t]
    \frametitle{Exercise \sweat{}}

    Can you row reduce 
    \begin{equation*}
        \begin{bmatrix}
            \color{gray}3 & \color{gray}-9 & \color{gray}12 & \color{gray}-9 & \color{gray}6 & \color{gray}15 \\
            0 & 2 & -4 & 4 & 2 & -6 \\
            0 & 3 & -6 & 6 & 4 & -5 \\
        \end{bmatrix}
    \end{equation*}
    and
    \begin{equation*}
        \begin{bmatrix}
            2 & 2 & 5 \\
            2 & 7 & -3 \\
            -2 & -5 & -1 \\
        \end{bmatrix}
    \end{equation*}
    to their reduced echelon forms?
\end{frame}

\end{document}
